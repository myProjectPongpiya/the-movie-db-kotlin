# Features

- Kotlin
- MVVM design pattern
- Retrofit
- LayoutManagers
- RecyclerView
- ViewPager2
- Service API Ref. https://developers.themoviedb.org/3/getting-started/introduction

# Demo

<img src="https://fv9-2.failiem.lv/thumb_show.php?i=gjtpk3ram&view" width="300"/> <img src="https://fv2-1.failiem.lv/thumb_show.php?i=hj6w22jvw&view&download_checksum=03561b4bcaeb422bee77d37563a395aca5ea0171&download_timestamp=1632289182" width="300"/> <img src="https://fv2-1.failiem.lv/thumb_show.php?i=nnvnygp99&view&download_checksum=1c848c3a6473259f0461525d5470c14b50e2f26d&download_timestamp=1632289216" width="300"/> <img src="https://fv2-1.failiem.lv/thumb_show.php?i=nb9znbzgz&view" width="300"/>

FILE APK : https://drive.google.com/drive/folders/1q66m4jccpnLcFmjV2InlecnwDJyRpDMm?usp=sharing

# Contact
email : clt.pongpiya@gmail.com, Thailand

# Announce
- CLOAN PROJECT CHECK OUT BRANCH 'DEVELOP'


