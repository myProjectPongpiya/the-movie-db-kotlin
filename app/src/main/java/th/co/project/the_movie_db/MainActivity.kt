package th.co.project.the_movie_db


import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleObserver
import androidx.navigation.NavController
import androidx.navigation.findNavController
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import th.co.project.the_movie_db.databinding.ActivityMainBinding
import th.co.project.the_movie_db.lib.eventbut.EventBus
import th.co.project.the_movie_db.lib.eventbut.ToggleLoading
import th.co.project.the_movie_db.ui.base.Alert
import th.co.project.the_movie_db.ui.base.AlertMessageDialog
import th.co.project.the_movie_db.ui.base.ProgressDialog
import th.co.project.the_movie_db.ui.component.home.HomeScreenFragment
import th.co.project.the_movie_db.ui.component.splash.SplashScreenFragment
import timber.log.Timber
import javax.inject.Inject


class MainActivity : AppCompatActivity(), HasAndroidInjector, LifecycleObserver {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    private val disposeBag: CompositeDisposable = CompositeDisposable()

    private lateinit var progressDialog: ProgressDialog
    lateinit var binding: ActivityMainBinding

    private lateinit var navController: NavController

    private var isActiveApp = true

    lateinit var splashScreenFragment: SplashScreenFragment
    lateinit var homeScreenFragment: HomeScreenFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        navController = findNavController(R.id.container)
        setupSmoothBottomMenu()
        progressDialog = ProgressDialog(this)
        lifecycle.addObserver(this)
        lifecycle.addObserver(progressDialog)

        EventBus.listen(Alert.OkDialog::class.java).subscribe {
            AlertMessageDialog.create(it).show(supportFragmentManager)
        }.also {
            disposeBag.add(it)
        }
        EventBus.listen(Alert.SelectDialog::class.java).subscribe {
            AlertMessageDialog.create(it).show(supportFragmentManager)
        }.also {
            disposeBag.add(it)
        }
        EventBus.listen(Alert.Toast::class.java).subscribe {
            AlertMessageDialog.showToast(this, it)
        }.also {
            disposeBag.add(it)
        }
        EventBus.listen(ToggleLoading::class.java)
            .subscribe {
                Timber.d("ToggleLoading :: %s", it)
                when (it) {
                    ToggleLoading.ShowProgress -> onLoading(true)
                    ToggleLoading.HideProgress -> onLoading(false)
                    else -> {
                    }
                }
            }.also {
                disposeBag.add(it)
            }

        val dm = resources.displayMetrics
        Timber.d("displayMetrics size = %s x %s", dm.widthPixels, dm.heightPixels)
        Timber.d(
            "displayMetrics density = %s, densityDpi = %s, scaledDensity = %s, xdpi=%s, ydpi=%s",
            dm.density,
            dm.densityDpi,
            dm.scaledDensity,
            dm.xdpi,
            dm.ydpi
        )

        visibilityNavElements(navController)
    }

    private fun setupSmoothBottomMenu() {
        val popupMenu = PopupMenu(this, null)
        popupMenu.inflate(R.menu.bottom_navigation_menu)
        val menu = popupMenu.menu
        binding.bottomNavigation.setupWithNavController(menu, navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    private fun visibilityNavElements(navController: NavController) {
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.splashFragment -> {
                    hideBottomNavigation()
                    window.statusBarColor = ContextCompat.getColor(this, R.color.yellow_splash)
                }
                R.id.listMovieFragment -> hideBottomNavigation()
                R.id.movieDetailFragment -> hideBottomNavigation()
                R.id.castDetailFragment -> hideBottomNavigation()
                else -> {
                    setUpStatusBar()
                    showBottomNavigation()
                }
            }
        }
    }

    private fun setUpStatusBar() {
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
    }

    fun hideBottomNavigation() {
        binding.bottomNavigation.visibility = View.GONE
    }

    fun showBottomNavigation() {
        binding.bottomNavigation.visibility = View.VISIBLE
    }

    private fun onLoading(show: Boolean) {
        Timber.d("onLoading %s", show)
        if (show) {
            progressDialog.showDialog()
        } else {
            progressDialog.hideDialog()
        }
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector


}