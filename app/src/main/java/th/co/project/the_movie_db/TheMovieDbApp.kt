package th.co.project.the_movie_db

import android.app.Application
import android.util.Log
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import th.co.project.the_movie_db.component.di.AppInjector
import th.co.project.the_movie_db.helper.AppPrefs
import timber.log.Timber
import timber.log.Timber.DebugTree
import javax.inject.Inject

class TheMovieDbApp : Application(), HasAndroidInjector {

//    init {
//        System.loadLibrary("coop-lib")
//    }
//
//    private external fun initCoopLib(): String

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this)
        initLog()
        AppPrefs.initEncryptedPrefs(this)
        AndroidThreeTen.init(this)

//        Timber.d("Sign APK : %s", initCoopLib())
//        Timber.d("ClientId %s", SecurityHelper.getApiClientId())
//        Timber.d("ClientSecret %s", SecurityHelper.getApiClientSecret())

    }

    private fun initLog() {
        if (BuildConfig.DEBUG) {
            Timber.plant(object : DebugTree() {
                override fun createStackElementTag(element: StackTraceElement): String? {
                    return (super.createStackElementTag(element) + " : "
                            + element.methodName + "\t"
                            + element.lineNumber)
                }
            })
        } else {
            Timber.plant(object : Timber.Tree() {
                private val CRASHLYTICS_KEY_PRIORITY = "priority"
                private val CRASHLYTICS_KEY_TAG = "tag"
                private val CRASHLYTICS_KEY_MESSAGE = "message"
                private var memberCode: String? = null
                override fun log(
                    priority: Int,
                    tag: String?,
                    message: String,
                    throwable: Throwable?
                ) {
//                    if (memberCode == null && AppPrefs.isInitial()) {
//                        memberCode = AppPrefs.getStaticString(AppConstant.MEMBER_CODE)
//                    }
                    if (priority == Log.ERROR || priority == Log.DEBUG) {
                        val crashlytics = FirebaseCrashlytics.getInstance()
                        crashlytics.log(message)
                        memberCode?.run {
                            crashlytics.setUserId(this)
                        }
                        tag?.run {
                            crashlytics.setCustomKey(CRASHLYTICS_KEY_TAG, tag)
                        }
                        throwable?.run {
                            crashlytics.recordException(throwable)
                        }
                    }
                }
            })
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
        }
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

}
