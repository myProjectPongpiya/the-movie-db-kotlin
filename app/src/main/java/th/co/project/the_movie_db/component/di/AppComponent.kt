package th.co.project.the_movie_db.component.di

import th.co.project.the_movie_db.TheMovieDbApp
import th.co.project.the_movie_db.component.di.module.AppModule
import th.co.project.the_movie_db.component.di.module.MainActivityModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        MainActivityModule::class]
)

interface AppComponent : AndroidInjector<TheMovieDbApp> {
    @Component.Factory
    interface Factory : AndroidInjector.Factory<TheMovieDbApp>
}
