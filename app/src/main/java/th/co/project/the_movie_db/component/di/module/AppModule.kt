package th.co.project.the_movie_db.component.di.module

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.threeten.bp.Clock
import org.threeten.bp.ZoneId
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import th.co.project.the_movie_db.BuildConfig
import th.co.project.the_movie_db.TheMovieDbApp
import th.co.project.the_movie_db.data.api.*
import th.co.project.the_movie_db.extension.addSslPinner
import th.co.project.the_movie_db.extension.addSslSocketFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideContext(mainApplication: TheMovieDbApp): Context = mainApplication.applicationContext

    @Singleton
    @Provides
    fun provideClock(): Clock = Clock.system(ZoneId.systemDefault())

    @Singleton
    @Provides
    fun provideRetrofit(
        context: Context,
        gson: Gson
    ): Retrofit.Builder {
        val builder = OkHttpClient().newBuilder()
        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(30, TimeUnit.SECONDS)
        builder.writeTimeout(30, TimeUnit.SECONDS)
        builder.addSslPinner(
            BuildConfig.API_ENDPOINT,
            BuildConfig.API_PIN1,
            BuildConfig.API_PIN2,
            BuildConfig.API_PIN3
        )
            .addSslSocketFactory(
                context,
                BuildConfig.API_PIN1,
                BuildConfig.API_PIN2,
                BuildConfig.API_PIN3
            )
        return Retrofit.Builder()
            .client(builder.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }

    @Singleton
    @Provides
    fun provideMovieDbAPI(retrofit: Retrofit.Builder, context: Context): TheMovieDBInterfaceAPI {
        val requestInterceptor = Interceptor { chain ->
            val url = chain.request()
                .url
                .newBuilder()
                .addQueryParameter("api_key", API_KEY)
                .build()

            val request = chain.request()
                .newBuilder()
                .url(url)
                .build()
            SERVICE = "${request.method}: ${request.url}"
            Timber.d("API | $SERVICE")

            return@Interceptor chain.proceed(request)
        }

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(requestInterceptor).addInterceptor(ChuckerInterceptor(context))
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()

        return retrofit
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TheMovieDBInterfaceAPI::class.java)
    }

    @Singleton
    @Provides
    fun provideMovieDbSearchAPI(
        retrofit: Retrofit.Builder,
        context: Context
    ): TheMovieDBInterfaceSearchAPI {
        val requestInterceptor = Interceptor { chain ->
            val url = chain.request()
                .url
                .newBuilder()
                .build()

            val request = chain.request()
                .newBuilder()
                .url(url)
                .build()
            SERVICE = "${request.method}: ${request.url}"
            Timber.d("API | $SERVICE")

            return@Interceptor chain.proceed(request)
        }

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(requestInterceptor).addInterceptor(ChuckerInterceptor(context))
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()

        return retrofit
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TheMovieDBInterfaceSearchAPI::class.java)
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            .create()
    }

}
