package th.co.project.the_movie_db.component.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import th.co.project.the_movie_db.ui.component.account.AccountFragment
import th.co.project.the_movie_db.ui.component.home.HomeScreenFragment
import th.co.project.the_movie_db.ui.component.home.detail.MovieDetailFragment
import th.co.project.the_movie_db.ui.component.home.detail.cast.CastDetailFragment
import th.co.project.the_movie_db.ui.component.list.ListMovieFragment
import th.co.project.the_movie_db.ui.component.splash.SplashScreenFragment

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeSplashFragment(): SplashScreenFragment

    @ContributesAndroidInjector
    abstract fun contributeHomeScreenFragment(): HomeScreenFragment

    @ContributesAndroidInjector
    abstract fun contributeBrandScreenFragment(): MovieDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeListMovieFragment(): ListMovieFragment

    @ContributesAndroidInjector
    abstract fun contributeCastDetailFragment(): CastDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeAccountFragment(): AccountFragment

}
