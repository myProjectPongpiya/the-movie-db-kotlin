package th.co.project.the_movie_db.component.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import th.co.project.the_movie_db.component.di.ViewModelKey
import th.co.project.the_movie_db.component.viewmodel.ViewModelFactory
import th.co.project.the_movie_db.ui.component.account.AccountViewModel
import th.co.project.the_movie_db.ui.component.home.HomeScreenViewModel
import th.co.project.the_movie_db.ui.component.home.detail.MovieDetailViewModel
import th.co.project.the_movie_db.ui.component.home.detail.cast.CastDetailViewModel
import th.co.project.the_movie_db.ui.component.list.ListMovieViewModel
import th.co.project.the_movie_db.ui.component.splash.SplashScreenViewModel

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(SplashScreenViewModel::class)
    abstract fun bindSplashScreenViewModel(screenViewModel: SplashScreenViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountViewModel::class)
    abstract fun bindAccountScreenViewModel(screenViewModel: AccountViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeScreenViewModel::class)
    abstract fun bindHomeScreenViewModel(screenViewModel: HomeScreenViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailViewModel::class)
    abstract fun bindBrandScreenViewModel(screenViewModel: MovieDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListMovieViewModel::class)
    abstract fun bindListMovieViewModel(listMovieViewModelViewModel: ListMovieViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CastDetailViewModel::class)
    abstract fun bindCastDetailViewModel(castDetailViewModelViewModel: CastDetailViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
