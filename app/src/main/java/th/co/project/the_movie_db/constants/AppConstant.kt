package th.co.project.the_movie_db.constants

class AppConstant {
    companion object {
        const val PICTURES_PATH = "pictures_path"

        const val CODE_INTERNET_CONNECT = "INTERNET_CONNECT"
        const val CODE_MS599ERROR = "MS599ERROR"
        const val CODE_DEVICE_INVALID = "MER005ERR"

        var IS_MARK_ACCOUNT = false

    }
}