package th.co.project.the_movie_db.constants

class AppConstants {
    companion object {
        const val MEDIA_TYPE_ALL = "all"
        const val MEDIA_TYPE_MOVIE = "movie"
        const val MEDIA_TYPE_TV = "tv"
        const val MEDIA_TYPE_PERSON = "person"

        const val TIME_WINDOW_DAY = "day"
        const val TIME_WINDOW_WEEK = "week"


    }
}