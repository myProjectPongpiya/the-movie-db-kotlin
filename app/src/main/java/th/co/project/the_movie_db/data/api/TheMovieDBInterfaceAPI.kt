package th.co.project.the_movie_db.data.api

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import th.co.project.the_movie_db.data.api.http.ApiPageResponse
import th.co.project.the_movie_db.model.movie_db.cast_credits.CastCreditsResponse
import th.co.project.the_movie_db.model.movie_db.cast_detail.CastDetailResponse
import th.co.project.the_movie_db.model.movie_db.credits.CreditsResponse
import th.co.project.the_movie_db.model.movie_db.detail.MovieDetailsResponse
import th.co.project.the_movie_db.model.movie_db.list.MovieList
import th.co.project.the_movie_db.model.movie_db.trending.TrendingResponse
import th.co.project.the_movie_db.model.movie_db.videos.VideosResponse


interface TheMovieDBInterfaceAPI {

    @GET("movie/{movie_id}")
    fun getMovieDetails(@Path("movie_id") id: Int): Observable<MovieDetailsResponse>

    @GET("trending/{media_type}/{time_window}")
    fun getTrending(
        @Path("media_type") mediaType: String,
        @Path("time_window") timeWindow: String
    ): Observable<ApiPageResponse<MutableList<TrendingResponse>>>

    @GET("movie/{movie_id}/lists")
    fun getMovieList(@Path("movie_id") id: Int): Observable<ApiPageResponse<MutableList<MovieList>>>

    @GET("movie/{movie_id}/credits")
    fun getMovieCredits(@Path("movie_id") id: Int): Observable<CreditsResponse>

    @GET("movie/{movie_id}/videos")
    fun getVideos(@Path("movie_id") id: Int): Observable<VideosResponse>

    @GET("person/{person_id}")
    fun getCastDetail(@Path("person_id") id: Int): Observable<CastDetailResponse>

    @GET("person/{person_id}/movie_credits")
    fun getCastCredits(@Path("person_id") id: Int): Observable<CastCreditsResponse>

}

