package th.co.project.the_movie_db.data.api

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import th.co.project.the_movie_db.data.api.http.ApiPageResponse
import th.co.project.the_movie_db.model.movie_db.detail.MovieDetailsResponse
import th.co.project.the_movie_db.model.movie_db.list.MovieList
import th.co.project.the_movie_db.model.movie_db.search.SearchMovieResponse
import th.co.project.the_movie_db.model.movie_db.search.SearchResponse
import th.co.project.the_movie_db.model.movie_db.trending.TrendingResponse


interface TheMovieDBInterfaceSearchAPI {

    @GET("search/movie?api_key=${API_KEY}")
    fun getSearchMovie(@Query("query") query: String): Observable<ApiPageResponse<MutableList<SearchMovieResponse>>>
}

