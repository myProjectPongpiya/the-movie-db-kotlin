package th.co.project.the_movie_db.data.api.http

import com.google.gson.annotations.SerializedName


data class ApiError(
    val code: String,
    val message: String,
    val httpCode: Int,
    val throwable: Throwable? = null
)

data class ApiResponse<T>(
    @SerializedName("code")
    val code: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("result")
    val result: T?
)

data class ApiPageResponse<T>(
    @SerializedName("id")
    val id: Int? = 0,
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val results: T?,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int,
)
