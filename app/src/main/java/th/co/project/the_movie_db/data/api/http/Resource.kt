package th.co.project.the_movie_db.data.api.http

import th.co.project.the_movie_db.data.api.http.Status.*

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}

data class Resource<out T>(val status: Status, val data: T?, val message: String?) {

    fun isSuccess(): Boolean {
        return status == SUCCESS
    }

    fun isError(): Boolean {
        return status == ERROR
    }

    fun isLoading(): Boolean {
        return status == LOADING
    }

    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(ERROR, data, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(LOADING, data, null)
        }

        fun <T> loading(): Resource<T> {
            return Resource(LOADING, null, null)
        }
    }
}
