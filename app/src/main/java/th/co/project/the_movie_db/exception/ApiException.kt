package th.co.project.the_movie_db.exception

class ApiException(
    val code: String,
    override val message: String
) : RuntimeException()