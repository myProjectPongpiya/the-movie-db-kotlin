package th.co.project.the_movie_db.exception

import java.io.IOException

class NoConnectivityException : IOException()