package th.co.project.the_movie_db.exception

sealed class TokenExpiredException : Exception() {

    object IsExpired : TokenExpiredException()
    data class IsApiException(val exception: ApiException) : TokenExpiredException()
    data class IsSystemError(val exception: Exception) : TokenExpiredException()

}
