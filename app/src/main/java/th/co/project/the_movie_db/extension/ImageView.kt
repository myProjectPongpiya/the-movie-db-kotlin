package th.co.project.the_movie_db.extension

import android.content.res.ColorStateList
import android.graphics.PorterDuff
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy.NONE
import com.bumptech.glide.load.model.GlideUrl
import th.co.project.the_movie_db.R


fun AppCompatImageView.loadRes(@DrawableRes drawableRes: Int) {
    Glide.with(this)
        .load(drawableRes)
        .diskCacheStrategy(NONE)
        .into(this)
}

fun AppCompatImageView.loadCircle(any: Any) {
    Glide.with(this).load(any)
        .circleCrop()
        .diskCacheStrategy(NONE)
        .into(this)
}

fun AppCompatImageView.loadUrl(
    url: GlideUrl,
    @DrawableRes placeholderRes: Int? = null,
    @DrawableRes onErrorRes: Int? = null
) {
    val builder = Glide
        .with(this)
        .load(url)
        .diskCacheStrategy(NONE)

    placeholderRes?.run {
        builder.placeholder(placeholderRes)
    }
    onErrorRes?.run {
        builder.error(this)
    }

    builder.into(this)
}

fun AppCompatImageView.setAccountIcon(coopAccountNo: String?, bayAccountNo: String?) {
    coopAccountNo?.run {
        loadCircle(R.drawable.logo_prior)
    } ?: bayAccountNo.run {
        loadCircle(R.drawable.logo_prior)
    }
}

fun AppCompatImageView.setColorTint(
    @ColorRes colorRes: Int,
    mode: PorterDuff.Mode? = PorterDuff.Mode.MULTIPLY
) {
//    setColorFilter(ContextCompat.getColor(context, colorRes), mode);

    ImageViewCompat.setImageTintList(this, ColorStateList.valueOf(ContextCompat.getColor(context, colorRes)));

}