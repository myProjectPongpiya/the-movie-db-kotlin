package th.co.project.the_movie_db.extension


import android.content.Context
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import timber.log.Timber
import java.net.URI
import java.security.cert.*
import javax.net.ssl.*


fun OkHttpClient.Builder.addSslPinner(
    url: String,
    pinRoot: String?,
    pinIntermediate: String?,
    pinLeaf: String?
): OkHttpClient.Builder {
    if (pinRoot.isNullOrEmpty() || pinIntermediate.isNullOrEmpty() || pinLeaf.isNullOrEmpty()) {
        return this
    }

    val uri = URI.create(url)
    val host = uri.host
    val certificatePinner = CertificatePinner.Builder()
        .add(host, pinRoot)
        .add(host, pinIntermediate)
        .add(host, pinLeaf)
        .build()
    this.certificatePinner(certificatePinner)
    return this
}

fun OkHttpClient.Builder.addSslSocketFactory(
    context: Context,
    pinRoot: String?,
    pinIntermediate: String?,
    pinLeaf: String?
): OkHttpClient.Builder {


//    try {
//        val cf: CertificateFactory = CertificateFactory.getInstance("X.509")
//        val caInput: InputStream = context.resources.openRawResource(R.raw.trusted)
//        var ca: Certificate? = null
//        try {
//            ca = cf.generateCertificate(caInput)
//        } catch (e: CertificateException) {
//            Timber.e(e)
//            return this
//        } finally {
//            caInput.close()
//        }
//
//        val keyStoreType: String = KeyStore.getDefaultType()
//        val keyStore: KeyStore = KeyStore.getInstance(keyStoreType)
//        keyStore.load(null, null)
//        if (ca == null) return this
//        keyStore.setCertificateEntry("ca", ca)
//
//        // Create a TrustManager that trusts the CAs in our KeyStore
//        val tmfAlgorithm: String = TrustManagerFactory.getDefaultAlgorithm()
//        val tmf: TrustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm)
//        tmf.init(keyStore)
//
//        // Create an SSLContext that uses our TrustManager
//        val sslContext: SSLContext = SSLContext.getInstance("TLS")
//        sslContext.init(null, tmf.trustManagers, null)
//        sslSocketFactory(sslContext.socketFactory)
//    } catch (e: Exception) {
//        Timber.e(e)
//    }
    if (pinRoot.isNullOrEmpty() || pinIntermediate.isNullOrEmpty() || pinLeaf.isNullOrEmpty()) {
        try {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
                }

                override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
                }

                override fun getAcceptedIssuers() = arrayOf<X509Certificate>()
            })

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory
            sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            hostnameVerifier { _, _ -> true }.build()
        } catch (e: Exception) {
            Timber.e(e)
        }
    }
    return this
}