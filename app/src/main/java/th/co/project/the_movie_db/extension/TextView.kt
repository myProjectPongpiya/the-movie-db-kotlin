package th.co.project.the_movie_db.extension

import android.os.Build
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Patterns
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.constants.AppConstant
import th.co.project.the_movie_db.helper.FormatHelper
import java.math.BigDecimal


fun TextView.string(@StringRes stringRds: Int) {
    text = context.getString(stringRds)
}

fun TextView.stringHTML(@StringRes stringRds: Int) {
    text = Html.fromHtml(context.getString(stringRds), Html.FROM_HTML_MODE_LEGACY)
}

fun TextView.textSize(@DimenRes sizeRds: Int) {
    textSize = context.resources.getDimension(sizeRds)
}

fun TextView.setColor(@ColorRes colorRes: Int) {
    setTextColor(ContextCompat.getColor(context, colorRes))
}

fun TextView.setFontTypeface(typeFace: Int) {
    setTypeface(typeface, typeFace)
}

fun TextView.setAmount(amount: BigDecimal?, showCurrency: Boolean = false, default: String = "") {
    text = amount?.let {
        FormatHelper.formatAmount(it) + " " + (if (showCurrency) getString(R.string.label_transaction_currency) else "")
    } ?: default

}

fun TextView.setAccountNo(
    coopAccountNo: String?,
    bayAccountNo: String?,
    masked: Boolean = AppConstant.IS_MARK_ACCOUNT
) {
    text = FormatHelper.formatAccountNo(
        accNumber = coopAccountNo ?: bayAccountNo ?: "",
        masked = masked
    )
}

fun TextView.setHtmlText(content: String, onClickURL: (url: String) -> Unit) {
    val links = HashMap<String, String>()
    val matcher = Patterns.WEB_URL.matcher(content)
    var end = 0
    var contextText = content

//    val temp = "XXXXXXXXXXXXXXX"
//    while (matcher.find()) {
//        val url: String = matcher.group()
//        val start = content.indexOf("<a")
//        Timber.d("URL extracted1: $url")
//        if (start > -1) {
//            end = content.indexOf("</a>")
//            var text = content.substring(start, end)
//            text = text.substring(text.indexOf("\">") + 2)
//
//            var key = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
//            } else {
//                Html.fromHtml(text)
//            }.toString()
//
//            key = "$temp$key"
//            contextText = contextText.replace(text, key)
//            Timber.d("URL extracted2: $text")
//
//            links[key] = url
//        } else {
//            links[url] = url
//        }
//
//    }

    var htmlText = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(contextText, Html.FROM_HTML_MODE_COMPACT)
    } else {
        Html.fromHtml(contextText)
    }.toString()
    if (links.isNullOrEmpty()) {
        text = htmlText
        return
    }


//    setText(htmlText)
//    links.keys.forEach {
//        val start = htmlText.indexOf(it)
//        if (start > -1 && start - temp.length > 0) {
//            htmlText = htmlText.replace(it, it.substring(temp.length))
//        }
//        val end = start + it.length
//        val key = it.substring(start + temp.length, end)
//        val url = links[it]?:""
//        Timber.d("URL extracted3 %s, %s", url , key)
//        SpannableString.valueOf(text)
//        val s = SpannableString.valueOf(htmlText)
//        s.setSpan(object : ClickableSpan(){
//            override fun onClick(p0: View) {
//                onClickURL.invoke(url)
//            }
//        }, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
//        text = s
//    }


//
//            val start: Int = string.indexOf(clickableText)
//            val end: Int = start + clickableText.length
//            Timber.d("clickableText : %s, %s - %s", clickableText, start, end)
//            if (start == -1) {
//                return
//            }
//
//            if (text is Spannable) {
//                text.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
//            } else {
//                val s = SpannableString.valueOf(text)
//                s.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
//                textView.text = s
//            }

    movementMethod = LinkMovementMethod.getInstance()
}