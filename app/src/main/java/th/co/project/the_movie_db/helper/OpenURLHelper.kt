package th.co.project.the_movie_db.helper

import android.content.Context
import android.net.Uri
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import th.co.project.the_movie_db.R
import timber.log.Timber

object OpenURLHelper {
    fun open(context: Context, url: String) {
        open(context, Uri.parse(url))
    }

    fun open(context: Context, url: Uri) {
        Timber.d("OpenURLHelper.open : %s", url)

        val customTabsIntent: CustomTabsIntent =
            CustomTabsIntent.Builder()
                .setShareState(CustomTabsIntent.SHARE_STATE_OFF)
                .setUrlBarHidingEnabled(false)
                .setDefaultColorSchemeParams(
                    CustomTabColorSchemeParams.Builder()
                        .setNavigationBarColor(ContextCompat.getColor(context, R.color.colorPrimary))
                        .setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary))
                        .setSecondaryToolbarColor(
                            ContextCompat.getColor(
                                context,
                                R.color.colorPrimaryDark
                            )
                        )
                        .build()
                )
                .setShowTitle(false).build()
        customTabsIntent.launchUrl(context, url)

//        val isOpened = CustomTabsClient.getPackageName(context, MutableListOf(TrustedWebUtils.EXTRA_LAUNCH_AS_TRUSTED_WEB_ACTIVITY, TrustedWebUtils.ACTION_MANAGE_TRUSTED_WEB_ACTIVITY_DATA), false)?.let { chromePackageName ->
//            val isBound = CustomTabsClient.connectAndInitialize(
//                context,
//                chromePackageName
//            )
//            Timber.d("OpenURLHelper.open : isBound = %s", isBound)
//            if (!isBound) {
//                Timber.d("OpenURLHelper.open : CustomTabsIntent")
//
//                true
//            } else {
//                Timber.d("OpenURLHelper.open1 : PublicWebActivity")
//                val publicWeb = Intent(context, PublicWebActivity::class.java)
//                publicWeb.putExtra("url", url)
//                context.startActivity(publicWeb)
//                true
//            }
//        }
//
//        if (isOpened != true) {
//            Timber.d("OpenURLHelper.open2 : PublicWebActivity")
//            val publicWeb = Intent(context, PublicWebActivity::class.java)
//            publicWeb.putExtra("url", url.toString())
//            context.startActivity(publicWeb)
//        }
    }

}