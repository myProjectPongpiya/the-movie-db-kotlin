package th.co.project.the_movie_db.helper

import android.util.Base64
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo
import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.openssl.PEMKeyPair
import org.bouncycastle.openssl.PEMParser
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import timber.log.Timber
import java.io.StringReader
import java.nio.ByteBuffer
import java.security.KeyPair
import java.security.PublicKey
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.Mac
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec


object SecurityHelper {

    external fun isRooted(): Boolean
    external fun getApiClientId(): String
    external fun getApiClientSecret(): String

    private external fun getApiPublicKey(): String
    private external fun getApiCipherAlgorithm(): String

    private external fun getHmacAlgorithm(): String
    private external fun getLocalEncryptAlgorithm(): String
    private external fun getLocalCipherAlgorithm(): String

    private external fun getLocalEncryptKeySize(): Int
    private external fun getLocalEncryptSecretSize(): Int

    private external fun getLocalEncryptKey(): ByteArray
    private external fun getLocalEncryptSecret(): ByteArray


    private val secureRandom = SecureRandom()

    @JvmStatic
    fun random(length: Int): ByteArray {
        val bytes = ByteArray(length)
        this.secureRandom.nextBytes(bytes)
        return bytes
    }

    fun getClientCredentials(): String {
        return "${getApiClientId()}:${getApiClientSecret()}"
    }

    fun toHmac(message: String): String {
        val mac = Mac.getInstance(getHmacAlgorithm())
        val secretKeySpec =
            SecretKeySpec(getClientCredentials().toByteArray(Charsets.UTF_8), getHmacAlgorithm())
        mac.init(secretKeySpec)
        val hmacSha256 = mac.doFinal(message.toByteArray(Charsets.UTF_8))
        return Base64.encodeToString(hmacSha256, Base64.NO_WRAP)
    }

    private fun publicKey(pemKey: String): PublicKey {
        val pem = PEMParser(StringReader(pemKey))
        val jcaPEMKeyConverter = JcaPEMKeyConverter()
        return when (val pemContent: Any = pem.readObject()) {
            is PEMKeyPair -> {
                val pemKeyPair: PEMKeyPair = pemContent
                val keyPair: KeyPair = jcaPEMKeyConverter.getKeyPair(pemKeyPair)
                keyPair.public
            }
            is SubjectPublicKeyInfo -> {
                val keyInfo: SubjectPublicKeyInfo = pemContent
                jcaPEMKeyConverter.getPublicKey(keyInfo)
            }
            is X509CertificateHolder -> {
                val cert: X509CertificateHolder = pemContent
                jcaPEMKeyConverter.getPublicKey(cert.subjectPublicKeyInfo)
            }
            else -> {
                throw IllegalArgumentException(
                    "Unsupported public key format '" +
                            pemContent.javaClass.simpleName + '"'
                )
            }
        }
    }

    fun encryptPinNo(pinNo: String): String {
        try {
            val cipher: Cipher = Cipher.getInstance(getApiCipherAlgorithm())
            cipher.init(Cipher.ENCRYPT_MODE, publicKey(getApiPublicKey()))
            return Base64.encodeToString(
                cipher.doFinal(pinNo.toByteArray(Charsets.UTF_8)),
                Base64.NO_WRAP
            )
        } catch (e: Exception) {
            Timber.e(e, "encryptPinNo")
        }
        return pinNo
    }


    fun generateAesKey(): ByteArray {
        return getLocalEncryptKey()
    }

    fun generateIV(): ByteArray {
        return getLocalEncryptSecret()
    }

    fun encryptText(text: String): String {
        return encryptText(generateAesKey(), generateIV(), text)
    }

    fun encryptText(aesKey: ByteArray, iv: ByteArray, text: String): String {
        try {
            val cipher = Cipher.getInstance(getLocalCipherAlgorithm())
            val aesKeySpec = SecretKeySpec(aesKey, getLocalEncryptAlgorithm())

            cipher.init(Cipher.ENCRYPT_MODE, aesKeySpec, IvParameterSpec(iv))
            val encryptText = cipher.doFinal(text.toByteArray(Charsets.UTF_8))
            val cipherText = ByteBuffer.allocate(aesKey.size + iv.size + encryptText.size).apply {
                put(aesKey)
                put(iv)
                put(encryptText)
            }.array()

            return Base64.encodeToString(cipherText, Base64.NO_WRAP)
        } catch (e: Exception) {
            Timber.e(e, "encryptText")
        }

        return text
    }

    fun decryptText(encryptText: String): String {
        if (encryptText.isNullOrEmpty()) return encryptText;
        val aesKey = ByteArray(getLocalEncryptKeySize())
        val iv = ByteArray(getLocalEncryptSecretSize())
        Timber.d("decryptText = %s", encryptText)
        val cipherText = ByteBuffer.wrap(Base64.decode(encryptText, Base64.NO_WRAP)).let {
            it.get(aesKey)
            it.get(iv)
            ByteArray(it.remaining()).apply {
                it.get(this)
            }
        }
        return decryptText(aesKey, iv, cipherText)
    }

    fun decryptText(aesKey: ByteArray, iv: ByteArray, encryptText: String): String {
        val aesKeyOriginal = ByteArray(getLocalEncryptKeySize())
        val ivOriginal = ByteArray(getLocalEncryptSecretSize())
        val cipherText = ByteBuffer.wrap(Base64.decode(encryptText, Base64.NO_WRAP)).let {
            it.get(aesKeyOriginal)
            it.get(ivOriginal)
            ByteArray(it.remaining()).apply {
                it.get(this)
            }
        }
        return decryptText(aesKey, iv, cipherText)
    }

    fun decryptText(aesKey: ByteArray, iv: ByteArray, cipherText: ByteArray): String {
        try {
            val cipher = Cipher.getInstance(getLocalCipherAlgorithm())
            val aesKeySpec = SecretKeySpec(aesKey, getLocalEncryptAlgorithm())
            cipher.init(Cipher.DECRYPT_MODE, aesKeySpec, IvParameterSpec(iv))
            return String(
                cipher.doFinal(cipherText),
                Charsets.UTF_8
            )
        } catch (e: Exception) {
            Timber.e(e, "decryptText")
        }

        return ""
    }

    fun checkPinDuplicateNumber(pinNo: String, duplicateCount: Int): Boolean {
        var latest = ""
        var count = 0
        for (c in pinNo.toCharArray()) {
            val c2 = c.toString()
            if (latest == c2) {
                count++
            } else {
                count = 1
            }
            latest = c2
            if (count >= duplicateCount) {
                return true
            }
        }
        return false
    }
}