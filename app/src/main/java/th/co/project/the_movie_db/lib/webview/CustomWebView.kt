package th.co.project.the_movie_db.lib.webview

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.webkit.JavascriptInterface
import android.webkit.WebView
import androidx.lifecycle.MutableLiveData


class CustomWebView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : WebView(context, attrs, defStyleAttr) {

    val webViewHeight: MutableLiveData<Int> = MutableLiveData()

    fun getContentVertical(): Int {
        return computeVerticalScrollRange()
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
//        requestDisallowInterceptTouchEvent(true);
        return super.onTouchEvent(event)
    }

    override fun onOverScrolled(scrollX: Int, scrollY: Int, clampedX: Boolean, clampedY: Boolean) {
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY)
//        requestDisallowInterceptTouchEvent(true)
    }

    var headerSize = 100

    @JavascriptInterface
    fun resize(height: Float) {
        var h = (height * resources.displayMetrics.density).toInt()
        if (h < headerSize) {
            h = headerSize
        }
        webViewHeight.postValue(h)
    }
}

