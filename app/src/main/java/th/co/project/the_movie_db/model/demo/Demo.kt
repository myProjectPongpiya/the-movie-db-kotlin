package th.co.project.the_movie_db.model.demo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DemoResponse(
    @SerializedName("image")
    val text: String? = null,
    val img : Int? = 0
) : Parcelable

