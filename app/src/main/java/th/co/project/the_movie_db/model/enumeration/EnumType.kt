package th.co.project.the_movie_db.model.enumeration

import com.google.gson.annotations.SerializedName

enum class ListType {
    @SerializedName("MOVIES")
    MOVIES,
    @SerializedName("TVS")
    TVS,
    @SerializedName("SEARCH")
    SEARCH,
    @SerializedName("NOT_FOUND")
    NOT_FOUND
}