package th.co.project.the_movie_db.model.movie_db.list


import com.google.gson.annotations.SerializedName

data class MovieList(
    @SerializedName("description")
    val description: String? = "",
    @SerializedName("favorite_count")
    val favoriteCount: Int? = 0,
    @SerializedName("id")
    val id: Int?=0,
    @SerializedName("item_count")
    val itemCount: Int?=0,
    @SerializedName("iso_639_1")
    val iso6391: String? = null,
    @SerializedName("list_type")
    val listType: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("poster_path")
    val posterPath: String? = null
)