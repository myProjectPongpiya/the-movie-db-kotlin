package th.co.project.the_movie_db.model.movie_db.search

import com.google.gson.annotations.SerializedName


data class SearchResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("logo_path")
    val logoPath: String? = null,
    @SerializedName("name")
    val name: String
)
