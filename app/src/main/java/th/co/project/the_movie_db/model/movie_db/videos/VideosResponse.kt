package th.co.project.the_movie_db.model.movie_db.videos

import com.google.gson.annotations.SerializedName


data class VideosResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("results")
    val results: MutableList<VideosList>
)

data class VideosList(
    @SerializedName("iso_639_1")
    val iso6391: String,
    @SerializedName("iso_3166_1")
    val iso31661: String,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("key")
    val key: String? = null,
    @SerializedName("site")
    val site: String,
    @SerializedName("size")
    val size: Int,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("official")
    val official: Boolean,
    @SerializedName("published_at")
    val publishedAt: String,
    @SerializedName("id")
    val id: String? = null
)