package th.co.project.the_movie_db.ui.base

import android.view.Gravity
import th.co.project.the_movie_db.R


sealed class Alert {
    data class Toast(
        val type: ToastType = ToastType.SUCCESS,
        val message: String,
        val gravity: Int = Gravity.TOP or Gravity.CENTER,
        val gravityBottom: Int = Gravity.BOTTOM or Gravity.CENTER
    ) : Alert()

    data class OkDialog(
        val code: String? = "9999",
        val title: String? = null,
        val message: String,
        val gravity: Int = Gravity.CENTER,
        val buttonLabel: String? = null,
        val buttonBg: Int = R.drawable.button_main_saving,
        val onDismiss: (() -> Unit)? = null,
        val cancelable: Boolean = false
    ) : Alert()

    data class SelectDialog(
        val code: String? = "9999",
        val title: String? = null,
        val message: String,
        val gravity: Int = Gravity.CENTER,
        val okButtonLabel: String? = null,
        val okButtonBg: Int = R.drawable.button_main_saving,
        val cancelButtonLabel: String? = null,
        val cancelButtonBg: Int = R.drawable.button_empty_saving,
        val onOkDismiss: (() -> Unit)? = null,
        val onCancelDismiss: (() -> Unit)? = null,
        val cancelable: Boolean = false
    ) : Alert()
}

enum class ToastType {
    SUCCESS,
    ERROR,
    WARNING
}
