package th.co.project.the_movie_db.ui.base


import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.FragmentManager
import th.co.project.the_movie_db.BuildConfig
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.extension.gone
import th.co.project.the_movie_db.extension.loadRes
import th.co.project.the_movie_db.extension.visible
import th.co.project.the_movie_db.ui.base.Alert.OkDialog
import th.co.project.the_movie_db.ui.base.Alert.SelectDialog
import timber.log.Timber

class AlertMessageDialog private constructor() : BaseDialog() {

    companion object {
        const val TAG = "AlertMessageDialog"
        private const val TITLE = "ALERT_TITLE"
        private const val LABEL_CODE = "LABEL_CODE"
        private const val LABEL_MESSAGE = "LABEL_MESSAGE"
        private const val LABEL_GRAVITY = "LABEL_GRAVITY"
        private const val BUTTON_OK = "BUTTON_OK"
        private const val BUTTON_CANCEL = "BUTTON_CANCEL"
        private const val BUTTON_OK_BG = "BUTTON_OK_BG"
        private const val BUTTON_CANCEL_BG = "BUTTON_CANCEL_BG"
        private const val DIALOG_TYPE = "DIALOG_TYPE"
        private const val DIALOG_CANCELABLE = "DIALOG_CANCELABLE"

        fun showToast(context: Context, alert: Alert.Toast) {
            val message = if (alert.message.isEmpty()) {
                context.getString(R.string.error_system)
            } else {
                alert.message
            }

            val layoutToast =
                LayoutInflater.from(context).inflate(R.layout.layout_toast, null)
//                Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
            layoutToast.findViewById<AppCompatTextView>(R.id.textMessage).text = message
            val icRes = when (alert.type) {
                ToastType.SUCCESS -> R.drawable.ic_checkbox_active
                ToastType.ERROR -> R.drawable.ic_checkbox_nope
                ToastType.WARNING -> R.drawable.ic_checkbox_active
            }
            layoutToast.findViewById<AppCompatImageView>(R.id.imgIcon).loadRes(icRes)
            val toast = Toast(context)
            toast.setGravity(
                alert.gravityBottom, 0,
                context.resources.getDimensionPixelOffset(R.dimen.margin16)
            )
            toast.duration = Toast.LENGTH_LONG
            toast.view = layoutToast
            toast.show()
        }

        fun create(
            alert: Alert
        ): AlertMessageDialog {
            val messageDialog =
                AlertMessageDialog()
            val args = Bundle()
            when (alert) {
                is OkDialog -> {
                    args.putString(LABEL_CODE, alert.code)
                    args.putString(LABEL_MESSAGE, alert.message)
                    args.putInt(LABEL_GRAVITY, alert.gravity)
                    args.putInt(BUTTON_OK_BG, alert.buttonBg)
                    alert.title?.run {
                        args.putString(TITLE, this)
                    }
                    alert.buttonLabel?.run {
                        args.putString(BUTTON_OK, this)
                    }
                    messageDialog.onOkDismiss = alert.onDismiss
                    args.putSerializable(
                        DIALOG_TYPE,
                        DialogType.OkDialog
                    );
                    args.putBoolean(DIALOG_CANCELABLE, alert.cancelable)
                }
                is SelectDialog -> {
                    args.putString(LABEL_CODE, alert.code)
                    args.putString(LABEL_MESSAGE, alert.message)
                    args.putInt(LABEL_GRAVITY, alert.gravity)
                    args.putInt(BUTTON_OK_BG, alert.okButtonBg)
                    args.putInt(BUTTON_CANCEL_BG, alert.cancelButtonBg)
                    alert.title?.run {
                        args.putString(TITLE, alert.title)
                    }
                    alert.okButtonLabel?.run {
                        args.putString(BUTTON_OK, this)
                    }
                    alert.cancelButtonLabel?.run {
                        args.putString(BUTTON_CANCEL, this)
                    }
                    messageDialog.onOkDismiss = alert.onOkDismiss
                    messageDialog.onCancelDismiss = alert.onCancelDismiss
                    args.putSerializable(
                        DIALOG_TYPE,
                        DialogType.SelectDialog
                    );
                    args.putBoolean(DIALOG_CANCELABLE, alert.cancelable)
                }
                else -> {

                }
            }
            messageDialog.apply {
                arguments = args
            }
            return messageDialog
        }
    }

    private enum class DialogType {
        OkDialog, SelectDialog
    }

    private var onOkDismiss: (() -> Unit)? = null
    private var onCancelDismiss: (() -> Unit)? = null

    fun show(manager: FragmentManager) {
        super.show(
            manager,
            TAG
        )
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        arguments?.run {
            dialog.setCancelable(getBoolean(DIALOG_CANCELABLE, false))
            isCancelable = getBoolean(DIALOG_CANCELABLE, false)
        }
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.dialog_alert_message, container, false)
        arguments?.run {
            initView(root, this)
        }
        return root
    }

    private fun initView(root: View, args: Bundle) {
        val title = args.getString(TITLE, "")
        val buttonOkLabel = args.getString(BUTTON_OK, "")
        val okButtonStyle = args.getInt(BUTTON_OK_BG, -1)
        val buttonCancelLabel = args.getString(BUTTON_CANCEL, "")
        val cancelButtonStyle = args.getInt(BUTTON_CANCEL_BG, -1)

        val dialogType = args.getSerializable(DIALOG_TYPE) as DialogType
        val gravity = args.getInt(LABEL_GRAVITY, Gravity.CENTER)
        val code = if (args.getString(LABEL_CODE, "").isEmpty()) {
            "9999"
        } else {
            args.getString(LABEL_CODE)
        }
        Timber.d("LABEL_MESSAGE : %s", args.getString(LABEL_MESSAGE))
        val message = if (args.getString(LABEL_MESSAGE, "").isEmpty()) {
            getString(R.string.error_system)
        } else {
            args.getString(LABEL_MESSAGE)
        }

        val textCode = root.findViewById<AppCompatTextView>(R.id.textCode)
        val textTitle = root.findViewById<AppCompatTextView>(R.id.textTitle)
        val textMessage = root.findViewById<AppCompatTextView>(R.id.textMessage)
        val buttonOk = root.findViewById<AppCompatButton>(R.id.buttonOk)
        val buttonCancel = root.findViewById<AppCompatButton>(R.id.buttonCancel)

        if (!title.isNullOrBlank()) {
            textTitle.text = title
        }

        if (okButtonStyle != -1) {
            buttonOk.setBackgroundResource(okButtonStyle)
        }
        if (cancelButtonStyle != -1) {
            buttonCancel.setBackgroundResource(cancelButtonStyle)
        }

        if (BuildConfig.DEBUG) {
            textCode.visible()
            textCode.text = code
        }

        textMessage.text =
            System.getProperty("line.separator")?.let { message?.replace("\\n", it) } ?: message

        textMessage.gravity = gravity

        buttonOk.setOnClickListener {
            dismiss()
            Timber.d("onOkDismiss = $onOkDismiss")
            onOkDismiss?.invoke()
        }
        if (dialogType == DialogType.OkDialog) {
            buttonCancel.gone()
            buttonOk.text =
                if (!buttonOkLabel.isNullOrBlank()) {
                    buttonOkLabel
                } else
                    getString(R.string.button_ok)
        } else {
            if (!buttonOkLabel.isNullOrBlank()) {
                buttonOk.text = buttonOkLabel
            }
            if (!buttonCancelLabel.isNullOrBlank()) {
                buttonCancel.text = buttonCancelLabel
            }
            buttonCancel.setOnClickListener {
                dismissAllowingStateLoss()
                onCancelDismiss?.invoke()
            }
        }
    }
}