package th.co.project.the_movie_db.ui.base


import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.appbar.MaterialToolbar
import th.co.project.the_movie_db.MainActivity
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.component.di.Injectable
import th.co.project.the_movie_db.component.util.autoCleared
import th.co.project.the_movie_db.extension.hideKeyboard
import th.co.project.the_movie_db.lib.eventbut.AppEvent
import th.co.project.the_movie_db.lib.eventbut.EventBus
import th.co.project.the_movie_db.lib.eventbut.ToggleLoading
import th.co.project.the_movie_db.lib.lifecycle.observeEvent
import th.co.project.the_movie_db.ui.base.binding.FragmentDataBindingComponent
import timber.log.Timber
import javax.inject.Inject


abstract class BaseFragment<FragmentDataBinding : ViewDataBinding,
        FragmentViewModel : BaseViewModel>
    : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    protected lateinit var viewModel: FragmentViewModel
    protected lateinit var dataBindingComponent: DataBindingComponent

    protected var dataBinding by autoCleared<FragmentDataBinding>()
    abstract val layoutId: Int

    abstract val viewModelClass: Class<FragmentViewModel>

    protected var toolbar: MaterialToolbar? = null
    protected var iconBack : ViewGroup? = null

    open fun onCreateView(rootView: View) {}

    abstract fun onFragmentStart()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.onAlertEvent.observeEvent(viewLifecycleOwner, this::onAlert)
        viewModel.onViewLoadingEvent.observeEvent(viewLifecycleOwner, this::onViewLoading)
        viewModel.onExitAppEvent.observeEvent(viewLifecycleOwner) {
            Timber.d("[EXIT]onExitAppEvent = %s", it)
            activity?.finish()
        }

        EventBus.listen(ToggleLoading::class.java)
            .subscribe {
                Timber.d("ToggleLoading :: %s", it)
                when (it) {
                    ToggleLoading.ShowViewLoading -> onViewLoading(true)
                    ToggleLoading.HideViewLoading -> onViewLoading(false)
                    else -> {
                    }
                }
            }.also {
                viewModel.addDisposableInternal(it)
            }
        /** new Create **/
        hideSoftKeyBoard()
        hideKeyboard()
        onFragmentStart()
    }

    /**
     * ทำให้ Keyboard ไม่บังช่องกรอก "id/eCoupon.xml"  **/
    override fun onResume() {
        val activity = this.activity as MainActivity?
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        super.onResume()
    }

    @SuppressLint("ClickableViewAccessibility")
    open fun hideSoftKeyBoard() {
        view?.setOnTouchListener { _, _ ->
            hideKeyboard()
            false
        }
    }

    open fun showOkAlert(
        code: String? = "",
        title: String? = null,
        message: String,
        onDismiss: (() -> Unit)? = null
    ) {
        viewModel.onAlertEvent.setEventValue(
            Alert.OkDialog(
                code = code,
                title = title,
                message = message,
                onDismiss = onDismiss
            )
        )
    }

    protected fun popBackStack() {
        Timber.d("popBackStack")
        findNavController().popBackStack()
    }

    protected fun popToRoot() {
        Timber.d("popToRoot")
        findNavController().navigateUp()
    }

    open fun onNavBackClick() {
        Timber.d("onNavBackClick")
        popBackStack()
    }

    protected fun onViewLoading(show: Boolean) {
//        if (show) {
//            dataBinding.root.findViewById<View>(R.id.loadingView)?.visible()
//        } else {
//            dataBinding.root.findViewById<View>(R.id.loadingView)?.gone()
//        }
    }

    protected fun onAlert(alert: Alert) {
        EventBus.publish(ToggleLoading.HideProgress)
        if (alert is Alert.Toast) {
            context?.run {
                AlertMessageDialog.showToast(this, alert)
            }
        } else {
            AlertMessageDialog.create(alert).show(childFragmentManager)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBindingComponent = FragmentDataBindingComponent(this)
        DataBindingUtil.setDefaultComponent(dataBindingComponent)
        dataBinding = DataBindingUtil.inflate(
            inflater,
            layoutId,
            container,
            false, dataBindingComponent
        )

        dataBinding.lifecycleOwner = this
        onCreateView(dataBinding.root)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(viewModelClass)
        dataBinding.lifecycleOwner = viewLifecycleOwner
        (viewModel as BaseViewModel).apply {
            gotoPage = {
                gotoPage(it)
            }
            popBackStack = {
                popBackStack()
            }
            popToRoot = {
                popToRoot()
            }
        }
        toolbar = dataBinding.root.findViewById(R.id.toolbar)
        toolbar?.setOnClickListener {
            onNavBackClick()
        }
        iconBack = dataBinding.root.findViewById(R.id.icBack)
        iconBack?.setOnClickListener {
            onNavBackClick()
        }
    }


    private fun navController() = findNavController()
    protected fun gotoPage(page: NavDirections) {
        Timber.d(
            "currentDestination = %s, page=%s",
            navController().currentDestination?.id,
            page.actionId
        )
        try {
            if (navController().currentDestination?.id == null) {
                onAlert(
                    Alert.OkDialog(
                        message = getString(R.string.err_refresh_app), onDismiss = {
                            EventBus.publish(AppEvent.ReloadApp)
                        })
                )
            } else {
                navController().navigate(page)
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    fun showNavBottom(fragment: Fragment, showNavBothBar: Boolean = true) {
        val activity = fragment.activity as MainActivity?
        if (showNavBothBar) activity?.showBottomNavigation() else activity?.hideBottomNavigation()
    }

    fun ViewPager2.autoScroll(interval: Long) {
        val handler = Handler()
        var scrollPosition = 0

        val runnable = object : Runnable {

            override fun run() {
                val count = adapter?.itemCount ?: 0
                setCurrentItem(scrollPosition++ % count, true)
                handler.postDelayed(this, interval)
            }
        }
        registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                scrollPosition = position + 1
            }

            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int,
            ) {
            }
        })
        handler.post(runnable)
    }

}



