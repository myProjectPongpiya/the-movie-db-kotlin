package th.co.project.the_movie_db.ui.base

import android.annotation.SuppressLint
import android.content.Context
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import th.co.project.the_movie_db.BuildConfig
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.constants.AppConstant
import th.co.project.the_movie_db.data.api.http.ApiError
import th.co.project.the_movie_db.exception.TokenExpiredException
import th.co.project.the_movie_db.helper.AppPrefs
import th.co.project.the_movie_db.lib.lifecycle.MutableLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject


abstract class BaseViewModel : ViewModel() {


    private var disposeBag: CompositeDisposable = CompositeDisposable()

    @SuppressLint("StaticFieldLeak")
    @Inject
    lateinit var appContext: Context

    val onAlertEvent: MutableLiveEvent<Alert> = MutableLiveEvent()
    val onLoadingEvent: MutableLiveEvent<Boolean> = MutableLiveEvent()
    val onViewLoadingEvent: MutableLiveEvent<Boolean> = MutableLiveEvent()
    val onExitAppEvent: MutableLiveEvent<String> = MutableLiveEvent()

    internal var gotoPage: ((page: NavDirections) -> Unit)? = null
    internal var popBackStack: (() -> Unit)? = null
    internal var popToRoot: (() -> Unit)? = null

    protected fun gotoPage(page: NavDirections) {
        gotoPage?.invoke(page)
    }

    protected fun popBackStack() {
        popBackStack?.invoke()
    }

    protected fun popToRoot() {
        popToRoot?.invoke()
    }

    open fun showOkAlert(
        titleRds: Int? = null,
        messageRds: Int,
        buttonLabelRes: Int? = null,
        onDismiss: () -> Unit
    ) {
        onAlertEvent.setEventValue(
            Alert.OkDialog(
                title = titleRds?.let { appContext.getString(it) },
                message = appContext.getString(messageRds),
                onDismiss = onDismiss,
                buttonLabel = buttonLabelRes?.let { appContext.getString(it) }
            )
        )
    }

    open fun showOkAlert(
        code: String? = "",
        title: String? = null,
        message: String,
        buttonBg: Int = R.drawable.button_main_saving,
        onDismiss: (() -> Unit)? = null
    ) {
        onAlertEvent.setEventValue(
            Alert.OkDialog(
                code = code,
                title = title,
                message = message,
                buttonBg = buttonBg,
                onDismiss = onDismiss
            )
        )
    }

    open fun showOkAlert(
        apiError: ApiError,
        buttonBg: Int = R.drawable.button_main_saving,
        onDismiss: (() -> Unit)? = null
    ) {
        if (BuildConfig.DEBUG && apiError.throwable != null) {
            Timber.e(apiError.throwable)
        }
        onAlertEvent.setEventValue(
            Alert.OkDialog(
                code = apiError.code,
                message = apiError.message,
                buttonBg = buttonBg,
                onDismiss = onDismiss
            )
        )
    }

    open fun showSelectAlert(
        apiError: ApiError,
        okButtonBg: Int = R.drawable.button_main_saving,
        cancelButtonBg: Int = R.drawable.button_empty_saving,
        onOkDismiss: (() -> Unit)? = null,
        onCancelDismiss: (() -> Unit)? = null
    ) {
        showSelectAlertMessage(
            code = apiError.code,
            message = apiError.message,
            okButtonBg = okButtonBg,
            cancelButtonBg = cancelButtonBg,
            onOkDismiss = onOkDismiss,
            onCancelDismiss = onCancelDismiss
        )
    }

    open fun showSelectAlertMessage(
        code: String? = null,
        title: String? = null,
        message: String,
        okButtonBg: Int = R.drawable.button_main_saving,
        cancelButtonBg: Int = R.drawable.button_empty_saving,
        onOkDismiss: (() -> Unit)? = null,
        onCancelDismiss: (() -> Unit)? = null
    ) {
        onAlertEvent.setEventValue(
            Alert.SelectDialog(
                code = code,
                title = title,
                message = message,
                okButtonBg = okButtonBg,
                cancelButtonBg = cancelButtonBg,
                onOkDismiss = onOkDismiss,
                onCancelDismiss = onCancelDismiss
            )
        )
    }

    protected open fun onApiError(apiError: ApiError) {
        onApiError(apiError, buttonBg = R.drawable.button_main_saving)
    }

    protected open fun onApiError(apiError: ApiError, buttonBg: Int = R.drawable.button_main_saving) {
        Timber.d("onApiError %s, %s", apiError.code, apiError.message)
        if (apiError.code == AppConstant.CODE_DEVICE_INVALID) {
            AppPrefs.clearAll()
            showOkAlert(apiError, buttonBg = buttonBg, onDismiss = {
                popToRoot()
            })
        } else {
            showOkAlert(apiError)
        }
    }

    internal fun callRefreshToken(e: TokenExpiredException) {

    }

    open fun showViewLoading() {
        this.onViewLoadingEvent.setEventValue(true)
    }

    open fun dismissViewLoading() {
        this.onViewLoadingEvent.setEventValue(false)
    }

    open fun showLoading() {
        this.onLoadingEvent.setEventValue(true)
    }

    open fun dismissLoading() {
        this.onLoadingEvent.setEventValue(false)
    }

    internal fun addDisposableInternal(d: Disposable) {
        this.disposeBag.add(d)
    }

    override fun onCleared() {
        disposeBag.clear()
        super.onCleared()
    }

    protected fun getString(@StringRes stringRes: Int): String {
        return appContext.getString(stringRes)
    }
}
