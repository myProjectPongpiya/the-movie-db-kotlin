package th.co.project.the_movie_db.ui.base.binding

import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import com.google.android.material.appbar.MaterialToolbar
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class FragmentBindingAdapters @Inject constructor(val fragment: Fragment) {

    @BindingAdapter("toDateString")
    fun toDateString(textView: AppCompatTextView, date: Date?) {
        if (date != null) {
            textView.text = toSimpleString(date)
        } else {
            textView.text = null
        }
    }

    private fun toSimpleString(date: Date): String {
        val format = SimpleDateFormat("dd/MM/yyyy")
        return format.format(date)
    }


    @BindingAdapter("navTheme")
    fun navTheme(toolbar: MaterialToolbar, isTransparent: Boolean?) {
        Timber.d("navTheme %s", isTransparent)
        toolbar.setBackgroundResource(
            if (isTransparent == true) {
                android.R.color.transparent
            } else {
                android.R.color.white
            }
        )


    }
}

