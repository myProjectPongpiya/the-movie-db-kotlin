package th.co.project.the_movie_db.ui.base.menubar

interface MenuBarListener {
    fun onBackClick()
}