package th.co.project.the_movie_db.ui.component.account

import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.databinding.FragmentAccountBinding
import th.co.project.the_movie_db.databinding.FragmentHomeBinding
import th.co.project.the_movie_db.ui.base.BaseFragment


class AccountFragment : BaseFragment<FragmentAccountBinding, AccountViewModel>() {

    override val layoutId: Int = R.layout.fragment_account

    override val viewModelClass: Class<AccountViewModel>
        get() = AccountViewModel::class.java


    override fun onFragmentStart() {

    }
}



