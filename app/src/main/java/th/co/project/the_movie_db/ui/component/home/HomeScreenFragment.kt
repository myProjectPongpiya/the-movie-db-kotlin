package th.co.project.the_movie_db.ui.component.home

import android.annotation.SuppressLint
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.constants.AppConstants
import th.co.project.the_movie_db.data.api.http.ApiPageResponse
import th.co.project.the_movie_db.databinding.FragmentHomeBinding
import th.co.project.the_movie_db.model.enumeration.ListType
import th.co.project.the_movie_db.model.movie_db.trending.TrendingResponse
import th.co.project.the_movie_db.ui.base.BaseFragment
import th.co.project.the_movie_db.ui.component.home.detail.MovieDetailContext
import th.co.project.the_movie_db.ui.component.list.ListMovieContext
import timber.log.Timber


class HomeScreenFragment : BaseFragment<FragmentHomeBinding, HomeScreenViewModel>() {

    override val layoutId: Int = R.layout.fragment_home

    override val viewModelClass: Class<HomeScreenViewModel>
        get() = HomeScreenViewModel::class.java

    private val select = ArrayList<Any>()

    override fun onFragmentStart() {
        dataBinding.fragment = this
        onTouchScrollView()
        onQueryMovieName()
        onRecyclerViewTrendingMovie()
        onRecyclerViewTitleTradingTvs()
        onAutoSlidePoster()
        onHandleScrollViewMoreItem()
        onClickViewMore()
    }

    private fun onAutoSlidePoster() {
        viewModel.getTrending(
            mediaType = AppConstants.MEDIA_TYPE_MOVIE,
            timeWindow = AppConstants.TIME_WINDOW_DAY,
            onSuccess = { trendingResponse ->
                dataBinding.viewPagerTitle.adapter = ViewPagerAutoScrollTitle(
                    onDemoItemClick = { _, position ->
                        onGotoPageMovieDetail(trendingResponse, position)
                    }
                ).apply {
                    add(trendingResponse.results ?: mutableListOf())
                }
                dataBinding.viewPagerTitle.autoScroll(3000)
            }
        )
    }

    private fun onRecyclerViewTrendingMovie() {
        viewModel.getTrending(
            mediaType = AppConstants.MEDIA_TYPE_MOVIE,
            timeWindow = AppConstants.TIME_WINDOW_DAY,
            onSuccess = { trendingResponse ->
                val countResponse = 8
                val row: Int = if (countResponse < 8) 1 else 2
                dataBinding.recyclerViewTrendingMovie.layoutManager =
                    GridLayoutManager(activity, row, GridLayoutManager.HORIZONTAL, false)
                dataBinding.recyclerViewTrendingMovie.adapter = RecyclerViewTrendingMovie(
                    onDemoItemClick = { _, position ->
                        trendingResponse.results?.get(position)?.let { select.add(it) }
                        Timber.d("log | select : $select")
                        onGotoPageMovieDetail(trendingResponse, position)
                    }
                ).apply {
                    if (trendingResponse.results?.size!! > 8) {
                        add(trendingResponse.results.subList(0, 8))
                    } else {
                        add(trendingResponse.results)
                    }
                }
            }
        )
    }

    private fun onHandleScrollViewMoreItem() {
        dataBinding.recyclerViewTrendingMovie.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollHorizontally(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
//                    gotoPage(
//                        HomeScreenFragmentDirections.gotoListMovieFragment()
//                    )
                }
            }
        })

        dataBinding.recyclerViewTrendingMovie.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (!recyclerView.canScrollVertically(1)) {

                }
            }
        })
    }


    private fun onRecyclerViewTitleTradingTvs() {
        viewModel.getTrending(
            mediaType = AppConstants.MEDIA_TYPE_TV,
            timeWindow = AppConstants.TIME_WINDOW_DAY,
            onSuccess = { trendingResponse ->
                val countResponse = 8
                val row: Int = if (countResponse < 8) 1 else 2
                dataBinding.recyclerViewTitleTendingTvs.layoutManager =
                    GridLayoutManager(activity, row, GridLayoutManager.HORIZONTAL, false)
                dataBinding.recyclerViewTitleTendingTvs.adapter = RecyclerViewTitleTradingTvs(
                    onDemoItemClick = { _, position ->
                        onGotoPageMovieDetail(trendingResponse, position)
                    }
                ).apply {
                    if (trendingResponse.results?.size!! > 8) {
                        add(trendingResponse.results.subList(0, 8))
                    } else {
                        add(trendingResponse.results)
                    }
                }
            }
        )
    }

    private fun onGotoPageMovieDetail(
        trendingResponse: ApiPageResponse<MutableList<TrendingResponse>>,
        position: Int
    ) {
        gotoPage(
            HomeScreenFragmentDirections.gotoMovieDetailFragment(
                MovieDetailContext(
                    movieId = trendingResponse.results?.get(position)?.id ?: 0,
                    imgPoster = trendingResponse.results?.get(position)?.posterPath
                        ?: "",
                    movieName = trendingResponse.results?.get(position)?.originalTitle
                        ?: trendingResponse.results?.get(position)?.originalName
                        ?: "",
                    date = trendingResponse.results?.get(position)?.releaseDate
                        ?: trendingResponse.results?.get(position)?.firstAirDate
                        ?: "",
                    voteAvg = trendingResponse.results?.get(position)?.voteAverage
                        ?: 0.0,
                    voteCount = trendingResponse.results?.get(position)?.voteCount
                        ?: 0,
                    overview = trendingResponse.results?.get(position)?.overview
                        ?: ""
                )
            )
        )
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun onTouchScrollView() {
        dataBinding.scrollView.setOnTouchListener { v, event ->
            dataBinding.searchView.clearFocus()
            false
        }
    }

    private fun onQueryMovieName() {
        dataBinding.searchView.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.getSearchMovie(
                    movieName = query,
                    onSuccess = { SearchMovieResponse ->
                        if (SearchMovieResponse.results.isNullOrEmpty()) {
                            Timber.d("API | ไม่พบข้อมูล")
                            gotoPage(
                                HomeScreenFragmentDirections.gotoListMovieFragment(
                                    ListMovieContext(
                                        listType = ListType.NOT_FOUND
                                    )
                                )
                            )
                        } else {
                            Timber.d("API | Size : ${SearchMovieResponse.results.size} searchMovieResponse: ${SearchMovieResponse.results}")
                            if (SearchMovieResponse.results.size == 1) {
                                gotoPage(
                                    HomeScreenFragmentDirections.gotoMovieDetailFragment(
                                        MovieDetailContext(
                                            movieId = SearchMovieResponse.results[0].id,
                                            imgPoster = SearchMovieResponse.results[0].posterPath?:"",
                                            movieName = SearchMovieResponse.results[0].originalTitle?:"",
                                            date = SearchMovieResponse.results[0].releaseDate,
                                            voteAvg = SearchMovieResponse.results[0].voteAverage,
                                            voteCount = SearchMovieResponse.results[0].voteCount,
                                            overview = SearchMovieResponse.results[0].overview
                                        )
                                    )
                                )
                            } else {
                                gotoPage(
                                    HomeScreenFragmentDirections.gotoListMovieFragment(
                                        ListMovieContext(
                                            listType = ListType.SEARCH,
                                            movieSearchName = query
                                        )
                                    )
                                )
                            }
                        }
                    }
                )
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }

    private fun onClickViewMore() {
        dataBinding.txtViewMoreTrendingMovie.setOnClickListener {
            gotoPage(
                HomeScreenFragmentDirections.gotoListMovieFragment(
                    ListMovieContext(
                        listType = ListType.MOVIES
                    )
                )
            )
        }

        dataBinding.txtViewMoreTrendingTvs.setOnClickListener {
            gotoPage(
                HomeScreenFragmentDirections.gotoListMovieFragment(
                    ListMovieContext(
                        listType = ListType.TVS
                    )
                )
            )
        }
    }
}



