package th.co.project.the_movie_db.ui.component.home

import th.co.project.the_movie_db.data.api.http.ApiPageResponse
import th.co.project.the_movie_db.extension.disposedBy
import th.co.project.the_movie_db.extension.doToggleLoading
import th.co.project.the_movie_db.extension.subscribeWithViewModel
import th.co.project.the_movie_db.model.movie_db.detail.MovieDetailsResponse
import th.co.project.the_movie_db.model.movie_db.search.SearchMovieResponse
import th.co.project.the_movie_db.model.movie_db.trending.TrendingResponse
import th.co.project.the_movie_db.ui.base.BaseViewModel
import th.co.project.the_movie_db.usecase.GetDetailUseCase
import th.co.project.the_movie_db.usecase.GetMovieListUseCase
import th.co.project.the_movie_db.usecase.GetSearchMovieUseCase
import th.co.project.the_movie_db.usecase.GetTrendingUseCase
import timber.log.Timber
import javax.inject.Inject

class HomeScreenViewModel @Inject constructor(
    private val getTrendingUseCase: GetTrendingUseCase,
    private val getDetailUseCase: GetDetailUseCase,
    private val getSearchMovieUseCase: GetSearchMovieUseCase
) : BaseViewModel() {

    fun getSearchMovie(
        movieName: String?="",
        onSuccess: (response: ApiPageResponse<MutableList<SearchMovieResponse>>) -> Unit
    ) {
        Timber.d("log search  value | $movieName")
        getSearchMovieUseCase.build(movieName?:"")
            .doToggleLoading(this)
            .subscribeWithViewModel(
                this,
                onSuccess,
                this::onApiError
            ).disposedBy(this)
    }

    fun getTrending(
        mediaType: String,
        timeWindow: String,
        onSuccess: (response: ApiPageResponse<MutableList<TrendingResponse>>) -> Unit
    ) {
        getTrendingUseCase.build(mediaType, timeWindow)
            .doToggleLoading(this)
            .subscribeWithViewModel(
                this,
                onSuccess,
                this::onApiError
            ).disposedBy(this)
    }

    fun getDetail(
        movieId: Int,
        onSuccess: (response: MovieDetailsResponse) -> Unit
    ) {
        getDetailUseCase.build(movieId)
            .doToggleLoading(this)
            .subscribeWithViewModel(
                this,
                onSuccess,
                this::onApiError
            ).disposedBy(this)
    }
}
