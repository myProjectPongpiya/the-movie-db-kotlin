package th.co.project.the_movie_db.ui.component.home

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.load.model.GlideUrl
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.data.api.POSTER_BASE_URL
import th.co.project.the_movie_db.databinding.ItemTrailerMovieBinding
import th.co.project.the_movie_db.extension.loadUrl
import th.co.project.the_movie_db.model.movie_db.trending.TrendingResponse
import th.co.project.the_movie_db.ui.base.BaseRecyclerViewAdapter

class RecyclerViewTrendingMovie(
    private val onDemoItemClick: (demoResponse: TrendingResponse, position: Int) -> Unit,
) : BaseRecyclerViewAdapter<TrendingResponse, ItemTrailerMovieBinding>() {
    override fun createBinding(parent: ViewGroup): ItemTrailerMovieBinding {
        return ItemTrailerMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }

    override fun bind(binding: ItemTrailerMovieBinding, position: Int, item: TrendingResponse) {
        binding.adapter = this
        binding.item = item
        binding.position = position

        checkImagePoster(binding, item)

        binding.txtNameMovie.text = item.originalTitle
        binding.txtSinceMovie.text = item.releaseDate
    }

    fun onItemClick(item: TrendingResponse, position: Int) {
        onDemoItemClick.invoke(item, position)
    }

    private fun checkImagePoster(binding: ItemTrailerMovieBinding, item: TrendingResponse) {
        val imgPoster = POSTER_BASE_URL + item.posterPath
        if (item.posterPath.isNullOrEmpty()) {
            binding.image.setImageResource(R.drawable.img_logo_app)
        } else {
            item.posterPath.let {
                binding.image.loadUrl(
                    GlideUrl(imgPoster),
                    placeholderRes = R.drawable.img_logo_app,
                    onErrorRes = R.drawable.img_logo_app
                )
            }
        }
    }
}
