package th.co.project.the_movie_db.ui.component.home

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.load.model.GlideUrl
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.data.api.POSTER_BASE_URL
import th.co.project.the_movie_db.databinding.ItemHomeAutoScrollBinding
import th.co.project.the_movie_db.extension.loadUrl
import th.co.project.the_movie_db.model.movie_db.trending.TrendingResponse
import th.co.project.the_movie_db.ui.base.BaseRecyclerViewAdapter
import timber.log.Timber

class ViewPagerAutoScrollTitle(
    private val onDemoItemClick: (demoResponse: TrendingResponse, position: Int) -> Unit
) :
    BaseRecyclerViewAdapter<TrendingResponse, ItemHomeAutoScrollBinding>() {

    override fun createBinding(parent: ViewGroup): ItemHomeAutoScrollBinding {
        return ItemHomeAutoScrollBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }

    @SuppressLint("SetTextI18n")
    override fun bind(binding: ItemHomeAutoScrollBinding, position: Int, item: TrendingResponse) {
        binding.adapter = this
        binding.item = item
        binding.position = position

        checkImagePoster(binding, item)

        if (item.title.isNullOrEmpty()) binding.textMovie.text = "Movie..." else binding.textMovie.text = item.originalTitle
        binding.textOverview.text = item.overview
        binding.textVote.text = item.voteAverage.toString()
    }

    fun onItemClick(item: TrendingResponse, position: Int) {
        onDemoItemClick.invoke(item, position)
    }

    private fun checkImagePoster(binding: ItemHomeAutoScrollBinding, item: TrendingResponse) {
        val imgPoster = POSTER_BASE_URL + item.posterPath
        if (item.posterPath.isNullOrEmpty()) {
            binding.image.setImageResource(R.drawable.img_logo_app)
        } else {
            binding.image.loadUrl(
                GlideUrl(imgPoster),
                placeholderRes = R.drawable.img_logo_app,
                onErrorRes = R.drawable.img_logo_app
            )
        }
    }
}