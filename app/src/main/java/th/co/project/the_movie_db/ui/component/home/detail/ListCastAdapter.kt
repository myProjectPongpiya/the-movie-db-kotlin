package th.co.project.the_movie_db.ui.component.home.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.load.model.GlideUrl
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.data.api.POSTER_BASE_URL
import th.co.project.the_movie_db.databinding.ItemListCastBinding
import th.co.project.the_movie_db.extension.loadUrl
import th.co.project.the_movie_db.model.movie_db.credits.CastList
import th.co.project.the_movie_db.model.movie_db.credits.CreditsResponse
import th.co.project.the_movie_db.ui.base.BaseRecyclerViewAdapter

class ListCastAdapter(
    private val onDemoItemClick: (demoResponse: CastList, position: Int) -> Unit,
) : BaseRecyclerViewAdapter<CastList, ItemListCastBinding>() {
    override fun createBinding(parent: ViewGroup): ItemListCastBinding {
        return ItemListCastBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    }

    override fun bind(binding: ItemListCastBinding, position: Int, item: CastList) {
        binding.adapter = this
        binding.item = item
        binding.position = position

        checkImagePoster(binding, item)

        binding.txtNameCast.text = item.name
    }

    fun onItemClick(item: CastList, position: Int) {
        onDemoItemClick.invoke(item, position)
    }

    private fun checkImagePoster(
        binding: ItemListCastBinding,
        item: CastList
    ) {
        val imgPoster = POSTER_BASE_URL + item.profilePath
        if (item.profilePath.isNullOrEmpty()) {
            binding.imagePeople.setImageResource(R.drawable.img_logo_app)
        } else {
            binding.imagePeople.loadUrl(
                GlideUrl(imgPoster),
                placeholderRes = R.drawable.img_logo_app,
                onErrorRes = R.drawable.img_logo_app
            )

        }
    }
}
