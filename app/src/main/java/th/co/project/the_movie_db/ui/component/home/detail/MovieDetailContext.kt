package th.co.project.the_movie_db.ui.component.home.detail

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieDetailContext(
    val movieId: Int,
    val imgPoster: String,
    val movieName: String,
    val date: String,
    val voteAvg: Double,
    val voteCount: Int,
    val overview: String
) : Parcelable

