package th.co.project.the_movie_db.ui.component.home.detail

import android.content.Intent
import android.net.Uri
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.model.GlideUrl
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.data.api.BASE_YOUTUBE
import th.co.project.the_movie_db.data.api.KEY_YOUTUBE
import th.co.project.the_movie_db.data.api.POSTER_BASE_URL
import th.co.project.the_movie_db.databinding.FragmentMovieDetailBinding
import th.co.project.the_movie_db.extension.gone
import th.co.project.the_movie_db.extension.loadUrl
import th.co.project.the_movie_db.extension.visible
import th.co.project.the_movie_db.ui.base.Alert
import th.co.project.the_movie_db.ui.base.AlertMessageDialog
import th.co.project.the_movie_db.ui.base.BaseFragment
import th.co.project.the_movie_db.ui.component.home.detail.cast.CastDetailContext
import timber.log.Timber

class MovieDetailFragment :
    BaseFragment<FragmentMovieDetailBinding, MovieDetailViewModel>() {

    override val layoutId: Int = R.layout.fragment_movie_detail

    override val viewModelClass: Class<MovieDetailViewModel>
        get() = MovieDetailViewModel::class.java

    private val args by navArgs<MovieDetailFragmentArgs>()

    var isLoading: Boolean = false

    override fun onFragmentStart() {
        dataBinding.fragment = this
        handlerFetchingDateView()
        watchYoutubeUrl()
    }

    private fun watchYoutubeUrl() {
        viewModel.getVideos(
            movieId = args.movieDetailContext.movieId,
            onSuccess = { response ->
                if (response.results.isNullOrEmpty()) {
                    dataBinding.frameWatching.gone()
                } else {
                    dataBinding.frameWatching.visible()
                    dataBinding.frameMovie.setOnClickListener {
                        AlertMessageDialog.create(
                            alert = Alert.SelectDialog(
                                message = getString(R.string.label_need_goto_web_site),
                                onOkDismiss = {
                                    KEY_YOUTUBE = response.results[0].key.toString()
                                    val urlLink =
                                        Intent(
                                            Intent.ACTION_VIEW,
                                            Uri.parse(BASE_YOUTUBE + KEY_YOUTUBE)
                                        )
                                    startActivity(urlLink)
                                }
                            )
                        ).show(childFragmentManager)
                    }
                }
            }
        )
    }

    private fun handlerFetchingDateView() {
        val imgPoster = POSTER_BASE_URL + args.movieDetailContext.imgPoster
        Timber.d("imgPoster : $imgPoster")
        dataBinding.imgPoster.loadUrl(
            GlideUrl(imgPoster),
            placeholderRes = R.drawable.img_logo_app,
            onErrorRes = R.drawable.img_logo_app
        )
        dataBinding.txtNameMovie.text = args.movieDetailContext.movieName
        dataBinding.txtDate.text = getString(R.string.label_date, args.movieDetailContext.date)
        dataBinding.txtVoteAvg.text = args.movieDetailContext.voteAvg.toString()
        dataBinding.txtVoteCount.text =
            getString(R.string.label_title_review, args.movieDetailContext.voteCount.toString())
        dataBinding.txtOverview.text = args.movieDetailContext.overview

        onRecyclerViewListCast()
    }

    private fun onRecyclerViewListCast() {
        viewModel.getCredits(
            movieId = args.movieDetailContext.movieId,
            onSuccess = { response ->
                dataBinding.recyclerViewCast.layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                dataBinding.recyclerViewCast.adapter = ListCastAdapter(
                    onDemoItemClick = { _, position ->
                        viewModel.getCastDetail(
                            personId = response.cast[position].id,
                            onSuccess = {
                                gotoPage(
                                    MovieDetailFragmentDirections.gotoCastDetailFragment(
                                        CastDetailContext(
                                            personId = it.id,
                                            profilePath = it.profilePath,
                                            name = it.name,
                                            alsoKnownAs = it.alsoKnownAs,
                                            birthday = it.birthday ?: "",
                                            placeOfBirth = it.placeOfBirth ?: "",
                                            knownForDepartment = it.knownForDepartment,
                                            biography = it.biography
                                        )
                                    )
                                )
                            }
                        )
                    }
                ).apply {
                    add(response.cast)
                }
            },
            onError = {
                dataBinding.labelCast.gone()
                dataBinding.recyclerViewCast.gone()
            }
        )
//        val scrollListener = object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
//                val visibleItemCount = layoutManager.childCount
//                val totalItemCount = layoutManager.itemCount
//                val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()
//                if (visibleItemCount + firstVisibleItem >= totalItemCount) {
//
//                }
//            }
//        }
//        dataBinding.recyclerViewCast.addOnScrollListener(scrollListener)
    }
}

