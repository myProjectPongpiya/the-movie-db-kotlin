package th.co.project.the_movie_db.ui.component.home.detail

import th.co.project.the_movie_db.data.api.http.ApiError
import th.co.project.the_movie_db.extension.disposedBy
import th.co.project.the_movie_db.extension.doToggleLoading
import th.co.project.the_movie_db.extension.subscribeWithViewModel
import th.co.project.the_movie_db.model.movie_db.cast_detail.CastDetailResponse
import th.co.project.the_movie_db.model.movie_db.credits.CreditsResponse
import th.co.project.the_movie_db.model.movie_db.videos.VideosResponse
import th.co.project.the_movie_db.ui.base.BaseViewModel
import th.co.project.the_movie_db.usecase.GetCastDetailUseCase
import th.co.project.the_movie_db.usecase.GetCreditsUseCase
import th.co.project.the_movie_db.usecase.GetVideosUseCase
import timber.log.Timber
import javax.inject.Inject

class MovieDetailViewModel @Inject constructor(
    private val getCreditsUseCase: GetCreditsUseCase,
    private val getVideosUseCase: GetVideosUseCase,
    private val getCastDetailUseCase: GetCastDetailUseCase
) : BaseViewModel() {

    fun getCredits(
        movieId: Int,
        onSuccess: (response: CreditsResponse) -> Unit,
        onError: (error: ApiError) -> Unit
    ) {
        Timber.d("log | movieId : $movieId")
        getCreditsUseCase.build(movieId)
            .doToggleLoading(this)
            .subscribeWithViewModel(
                this,
                onSuccess,
                onError
            ).disposedBy(this)
    }

    fun getVideos(
        movieId: Int,
        onSuccess: (response: VideosResponse) -> Unit
    ) {
        Timber.d("log | movieId : $movieId")
        getVideosUseCase.build(movieId)
            .doToggleLoading(this)
            .subscribeWithViewModel(
                this,
                onSuccess,
                this::onApiError
            ).disposedBy(this)
    }

    fun getCastDetail(
        personId: Int,
        onSuccess: (response: CastDetailResponse) -> Unit
    ) {
        Timber.d("log | personId : $personId")
        getCastDetailUseCase.build(personId)
            .doToggleLoading(this)
            .subscribeWithViewModel(
                this,
                onSuccess,
                this::onApiError
            ).disposedBy(this)
    }
}
