package th.co.project.the_movie_db.ui.component.home.detail.cast

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CastDetailContext(
    val personId: Int,
    val profilePath: String? = null,
    val name: String,
    val alsoKnownAs: List<String>,
    val birthday: String,
    val placeOfBirth: String,
    val knownForDepartment: String,
    val biography: String? = null,
) : Parcelable

