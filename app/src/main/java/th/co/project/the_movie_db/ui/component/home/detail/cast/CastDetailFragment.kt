package th.co.project.the_movie_db.ui.component.home.detail.cast

import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.load.model.GlideUrl
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.data.api.POSTER_BASE_URL
import th.co.project.the_movie_db.databinding.FragmentCastDetailBinding
import th.co.project.the_movie_db.extension.gone
import th.co.project.the_movie_db.extension.loadUrl
import th.co.project.the_movie_db.ui.base.BaseFragment

class CastDetailFragment :
    BaseFragment<FragmentCastDetailBinding, CastDetailViewModel>() {

    override val layoutId: Int = R.layout.fragment_cast_detail

    override val viewModelClass: Class<CastDetailViewModel>
        get() = CastDetailViewModel::class.java

    private val args by navArgs<CastDetailFragmentArgs>()

    override fun onFragmentStart() {
        dataBinding.fragment = this
        setLayoutData()
    }

    private fun setLayoutData() {
        handleImageProfile()
        handleData()
    }

    private fun handleImageProfile() {
        val profilePath = args.castDetailContext.profilePath
        val imgPoster = POSTER_BASE_URL + profilePath
        if (profilePath.isNullOrEmpty()) {
            dataBinding.imgPerson.setImageResource(R.drawable.img_logo_app)
        } else {
            dataBinding.imgPerson.loadUrl(
                GlideUrl(imgPoster),
                placeholderRes = R.drawable.img_logo_app,
                onErrorRes = R.drawable.img_logo_app
            )
        }
    }

    private fun handleData() {
        val castDetailContext = args.castDetailContext
        dataBinding.txtNameCast.text = castDetailContext.name
        if (castDetailContext.alsoKnownAs.isNullOrEmpty()) {
            dataBinding.txtAlsoKnownAs.gone()
        } else dataBinding.txtAlsoKnownAs.text = castDetailContext.alsoKnownAs[0]
        dataBinding.txtBirthday.text = castDetailContext.birthday
        dataBinding.txtPlaceOfBirth.text = castDetailContext.placeOfBirth
        dataBinding.txtKnownForDepartment.text = castDetailContext.knownForDepartment
        if (castDetailContext.biography.isNullOrEmpty()) {
            dataBinding.labelBiography.gone()
            dataBinding.txtBiography.gone()
        } else dataBinding.txtBiography.text = castDetailContext.biography
        onRecyclerViewListCast()
    }

    private fun onRecyclerViewListCast() {
        viewModel.getCastCredits(
            personId = args.castDetailContext.personId,
            onSuccess = { response ->
                dataBinding.recyclerViewFilmography.layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                dataBinding.recyclerViewFilmography.adapter = ListCastCreditsAdapter(
                    onDemoItemClick = { _, position ->
                    }
                ).apply {
                    add(response.cast)
                }
            },
            onError = {
                dataBinding.labelFilmography.gone()
                dataBinding.recyclerViewFilmography.gone()
            }
        )
    }
}

