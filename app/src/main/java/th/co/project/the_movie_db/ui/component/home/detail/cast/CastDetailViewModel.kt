package th.co.project.the_movie_db.ui.component.home.detail.cast

import th.co.project.the_movie_db.data.api.http.ApiError
import th.co.project.the_movie_db.extension.disposedBy
import th.co.project.the_movie_db.extension.doToggleLoading
import th.co.project.the_movie_db.extension.subscribeWithViewModel
import th.co.project.the_movie_db.model.movie_db.cast_credits.CastCreditsResponse
import th.co.project.the_movie_db.ui.base.BaseViewModel
import th.co.project.the_movie_db.usecase.GetCastCreditsUseCase
import timber.log.Timber
import javax.inject.Inject

class CastDetailViewModel @Inject constructor(
    private val getCastCreditsUseCase: GetCastCreditsUseCase
) : BaseViewModel() {

    fun getCastCredits(
        personId: Int,
        onSuccess: (response: CastCreditsResponse) -> Unit,
        onError: (error: ApiError) -> Unit
    ) {
        Timber.d("log | movieId : $personId")
        getCastCreditsUseCase.build(personId)
            .doToggleLoading(this)
            .subscribeWithViewModel(
                this,
                onSuccess,
                onError
            ).disposedBy(this)
    }
}
