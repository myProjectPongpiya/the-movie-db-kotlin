package th.co.project.the_movie_db.ui.component.home.detail.cast

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.load.model.GlideUrl
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.data.api.POSTER_BASE_URL
import th.co.project.the_movie_db.databinding.ItemListCastCreditsBinding
import th.co.project.the_movie_db.extension.loadUrl
import th.co.project.the_movie_db.model.movie_db.cast_credits.CastCreditsList
import th.co.project.the_movie_db.model.movie_db.cast_credits.CastCreditsResponse
import th.co.project.the_movie_db.ui.base.BaseRecyclerViewAdapter

class ListCastCreditsAdapter(
    private val onDemoItemClick: (demoResponse: CastCreditsList, position: Int) -> Unit,
) : BaseRecyclerViewAdapter<CastCreditsList, ItemListCastCreditsBinding>() {
    override fun createBinding(parent: ViewGroup): ItemListCastCreditsBinding {
        return ItemListCastCreditsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    }

    override fun bind(
        binding: ItemListCastCreditsBinding,
        position: Int,
        item: CastCreditsList
    ) {
        binding.adapter = this
        binding.item = item
        binding.position = position

        checkImagePoster(binding, item)
        binding.txtNameMovie.text = item.title
    }

    fun onItemClick(item: CastCreditsList, position: Int) {
        onDemoItemClick.invoke(item, position)
    }

    private fun checkImagePoster(
        binding: ItemListCastCreditsBinding,
        item: CastCreditsList
    ) {
        val imgPoster = POSTER_BASE_URL + item.posterPath
        if (item.posterPath.isNullOrEmpty()) {
            binding.imageMovie.setImageResource(R.drawable.img_logo_app)
        } else {
            binding.imageMovie.loadUrl(
                GlideUrl(imgPoster),
                placeholderRes = R.drawable.img_logo_app,
                onErrorRes = R.drawable.img_logo_app
            )
        }
    }
}
