package th.co.project.the_movie_db.ui.component.list

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.load.model.GlideUrl
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.data.api.POSTER_BASE_URL
import th.co.project.the_movie_db.databinding.ItemMovieGridBinding
import th.co.project.the_movie_db.databinding.ItemMovieSearchGridBinding
import th.co.project.the_movie_db.extension.loadUrl
import th.co.project.the_movie_db.model.movie_db.search.SearchMovieResponse
import th.co.project.the_movie_db.model.movie_db.trending.TrendingResponse
import th.co.project.the_movie_db.ui.base.BaseRecyclerViewAdapter

class GridSearchMovieAdapter(
    private val onDemoItemClick: (demoResponse: SearchMovieResponse, position: Int) -> Unit,
) : BaseRecyclerViewAdapter<SearchMovieResponse, ItemMovieSearchGridBinding>() {
    override fun createBinding(parent: ViewGroup): ItemMovieSearchGridBinding {
        return ItemMovieSearchGridBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    }

    override fun bind(
        binding: ItemMovieSearchGridBinding,
        position: Int,
        item: SearchMovieResponse
    ) {
        binding.adapter = this
        binding.item = item
        binding.position = position

        checkImagePoster(binding, item)

        if (item.originalTitle.isNullOrEmpty()) binding.txtNameMovie.text = item.title
        else binding.txtNameMovie.text = item.originalTitle
    }

    fun onItemClick(item: SearchMovieResponse, position: Int) {
        onDemoItemClick.invoke(item, position)
    }

    private fun checkImagePoster(binding: ItemMovieSearchGridBinding, item: SearchMovieResponse) {
        val imgPoster = POSTER_BASE_URL + item.posterPath
        if (item.posterPath.isNullOrEmpty()) {
            binding.image.setImageResource(R.drawable.img_logo_app)
        } else {
            item.posterPath.let {
                binding.image.loadUrl(
                    GlideUrl(imgPoster),
                    placeholderRes = R.drawable.img_logo_app,
                    onErrorRes = R.drawable.img_logo_app
                )
            }
        }
    }
}
