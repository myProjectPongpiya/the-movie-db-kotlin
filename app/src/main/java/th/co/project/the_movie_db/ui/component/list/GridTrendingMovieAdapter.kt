package th.co.project.the_movie_db.ui.component.list

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.load.model.GlideUrl
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.data.api.POSTER_BASE_URL
import th.co.project.the_movie_db.databinding.ItemMovieGridBinding
import th.co.project.the_movie_db.extension.loadUrl
import th.co.project.the_movie_db.model.movie_db.trending.TrendingResponse
import th.co.project.the_movie_db.ui.base.BaseRecyclerViewAdapter

class GridTrendingMovieAdapter(
    private val onDemoItemClick: (demoResponse: TrendingResponse, position: Int) -> Unit,
) : BaseRecyclerViewAdapter<TrendingResponse, ItemMovieGridBinding>() {
    override fun createBinding(parent: ViewGroup): ItemMovieGridBinding {
        return ItemMovieGridBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }

    override fun bind(binding: ItemMovieGridBinding, position: Int, item: TrendingResponse) {
        binding.adapter = this
        binding.item = item
        binding.position = position

        checkImagePoster(binding, item)

        if (item.originalTitle.isNullOrEmpty()) binding.txtNameMovie.text = item.originalName
        else binding.txtNameMovie.text = item.originalTitle
        if (item.firstAirDate.isNullOrEmpty()) binding.txtSinceMovie.text = item.releaseDate
        else binding.txtSinceMovie.text = item.firstAirDate
    }

    fun onItemClick(item: TrendingResponse, position: Int) {
        onDemoItemClick.invoke(item, position)
    }

    private fun checkImagePoster(binding: ItemMovieGridBinding, item: TrendingResponse) {
        val imgPoster = POSTER_BASE_URL + item.posterPath
        if (item.posterPath.isNullOrEmpty()) {
            binding.image.setImageResource(R.drawable.img_logo_app)
        } else {
            item.posterPath.let {
                binding.image.loadUrl(
                    GlideUrl(imgPoster),
                    placeholderRes = R.drawable.img_logo_app,
                    onErrorRes = R.drawable.img_logo_app
                )
            }
        }
    }
}
