package th.co.project.the_movie_db.ui.component.list

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import th.co.project.the_movie_db.model.enumeration.ListType

@Parcelize
data class ListMovieContext(
    val listType: ListType,
    val movieSearchName: String? = null
) : Parcelable

