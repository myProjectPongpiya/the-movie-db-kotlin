package th.co.project.the_movie_db.ui.component.list

import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.constants.AppConstants
import th.co.project.the_movie_db.data.api.http.ApiPageResponse
import th.co.project.the_movie_db.databinding.FragmentMovieListBinding
import th.co.project.the_movie_db.extension.gone
import th.co.project.the_movie_db.extension.visible
import th.co.project.the_movie_db.model.enumeration.ListType
import th.co.project.the_movie_db.model.movie_db.search.SearchMovieResponse
import th.co.project.the_movie_db.model.movie_db.trending.TrendingResponse
import th.co.project.the_movie_db.ui.base.BaseFragment
import th.co.project.the_movie_db.ui.component.home.detail.MovieDetailContext

class ListMovieFragment :
    BaseFragment<FragmentMovieListBinding, ListMovieViewModel>() {

    override val layoutId: Int = R.layout.fragment_movie_list

    override val viewModelClass: Class<ListMovieViewModel>
        get() = ListMovieViewModel::class.java

    private val args by navArgs<ListMovieFragmentArgs>()

    override fun onFragmentStart() {
        dataBinding.fragment = this
        onFirstStartTitleBarAndService()
        onClickBtnMenu()
    }

    private fun onFirstStartTitleBarAndService() {
        when (args.listMovieContext.listType) {
            ListType.MOVIES -> {
                dataBinding.textBar.text = getString(R.string.label_movie)
                onShowListMovie()
            }
            ListType.TVS -> {
                dataBinding.textBar.text = getString(R.string.label_tvs)
                onShowListTvs()
            }
            ListType.SEARCH -> {
                dataBinding.textBar.text = getString(R.string.label_search)
                onShowListSearch()
            }
            ListType.NOT_FOUND -> {
                dataBinding.mainBar.gone()
                dataBinding.layoutEmptyNotFound.visible()
            }
            else -> showOkAlert(message = "${args.listMovieContext.listType}")
        }
    }

    private fun onClickBtnMenu() {
        val listType = args.listMovieContext.listType
        dataBinding.btnMenuGrid.setOnClickListener {
            handleOnClickBtnMenuGrid()
            when (listType) {
                ListType.MOVIES -> onShowGridMovie()
                ListType.TVS -> onShowGridTvs()
                ListType.SEARCH -> onShowGridSearch()
                else -> showOkAlert(message = "$listType")
            }
        }
        dataBinding.btnMenuHam.setOnClickListener {
            handleOnClickBtnMenuHam()
            when (listType) {
                ListType.MOVIES -> onShowListMovie()
                ListType.TVS -> onShowListTvs()
                ListType.SEARCH -> onShowListSearch()
                else -> showOkAlert(message = "$listType")
            }
        }
    }

    private fun handleOnClickBtnMenuGrid() {
        dataBinding.btnMenuGrid.gone()
        dataBinding.btnMenuHam.visible()
        dataBinding.recyclerViewList.gone()
        dataBinding.recyclerViewGrid.visible()
    }

    private fun handleOnClickBtnMenuHam() {
        dataBinding.btnMenuGrid.visible()
        dataBinding.btnMenuHam.gone()
        dataBinding.recyclerViewList.visible()
        dataBinding.recyclerViewGrid.gone()
    }

    private fun onShowListMovie() {
        viewModel.getTrending(
            mediaType = AppConstants.MEDIA_TYPE_MOVIE,
            timeWindow = AppConstants.TIME_WINDOW_DAY,
            onSuccess = { trendingResponse ->
                onRecyclerViewListLayout(trendingResponse)
            }
        )
    }

    private fun onShowListTvs() {
        viewModel.getTrending(
            mediaType = AppConstants.MEDIA_TYPE_TV,
            timeWindow = AppConstants.TIME_WINDOW_DAY,
            onSuccess = { trendingResponse ->
                onRecyclerViewListLayout(trendingResponse)
            }
        )
    }

    private fun onShowListSearch() {
        viewModel.getSearchMovie(
            movieName = args.listMovieContext.movieSearchName,
            onSuccess = { SearchMovieResponse ->
                onRecyclerViewListSearchLayout(SearchMovieResponse)
            }
        )
    }

    private fun onShowGridMovie() {
        viewModel.getTrending(
            mediaType = AppConstants.MEDIA_TYPE_MOVIE,
            timeWindow = AppConstants.TIME_WINDOW_DAY,
            onSuccess = { trendingResponse ->
                onRecyclerViewGridLayout(trendingResponse)
            }
        )
    }

    private fun onShowGridTvs() {
        viewModel.getTrending(
            mediaType = AppConstants.MEDIA_TYPE_TV,
            timeWindow = AppConstants.TIME_WINDOW_DAY,
            onSuccess = { trendingResponse ->
                onRecyclerViewGridLayout(trendingResponse)
            }
        )
    }

    private fun onShowGridSearch() {
        viewModel.getSearchMovie(
            movieName = args.listMovieContext.movieSearchName,
            onSuccess = { SearchMovieResponse ->
                onRecyclerViewGridSearchLayout(SearchMovieResponse)
            }
        )
    }

    private fun onRecyclerViewListLayout(trendingResponse: ApiPageResponse<MutableList<TrendingResponse>>) {
        dataBinding.recyclerViewList.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        dataBinding.recyclerViewList.adapter = ListTrendingMovieAdapter(
            onDemoItemClick = { _, position ->
                gotoPage(
                    ListMovieFragmentDirections.gotoMovieDetailFragment(
                        MovieDetailContext(
                            movieId = trendingResponse.results?.get(position)?.id ?: 0,
                            imgPoster = trendingResponse.results?.get(position)?.posterPath ?: "",
                            movieName = trendingResponse.results?.get(position)?.originalTitle
                                ?: trendingResponse.results?.get(position)?.originalName ?: "",
                            date = trendingResponse.results?.get(position)?.releaseDate
                                ?: trendingResponse.results?.get(position)?.firstAirDate ?: "",
                            voteAvg = trendingResponse.results?.get(position)?.voteAverage ?: 0.0,
                            voteCount = trendingResponse.results?.get(position)?.voteCount ?: 0,
                            overview = trendingResponse.results?.get(position)?.overview ?: ""
                        )
                    )
                )
            }
        ).apply {
            add(trendingResponse.results ?: mutableListOf())
        }
    }

    private fun onRecyclerViewListSearchLayout(searchMovieResponse: ApiPageResponse<MutableList<SearchMovieResponse>>) {
        dataBinding.recyclerViewList.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        dataBinding.recyclerViewList.adapter = ListSearchMovieAdapter(
            onDemoItemClick = { _, position ->
                gotoPage(
                    ListMovieFragmentDirections.gotoMovieDetailFragment(
                        MovieDetailContext(
                            movieId = searchMovieResponse.results?.get(position)?.id ?: 0,
                            imgPoster = searchMovieResponse.results?.get(position)?.posterPath
                                ?: "",
                            movieName = searchMovieResponse.results?.get(position)?.originalTitle
                                ?: "",
                            date = searchMovieResponse.results?.get(position)?.releaseDate ?: "",
                            voteAvg = searchMovieResponse.results?.get(position)?.voteAverage
                                ?: 0.0,
                            voteCount = searchMovieResponse.results?.get(position)?.voteCount ?: 0,
                            overview = searchMovieResponse.results?.get(position)?.overview ?: ""
                        )
                    )
                )
            }
        ).apply {
            add(searchMovieResponse.results ?: mutableListOf())
        }
    }

    private fun onRecyclerViewGridLayout(trendingResponse: ApiPageResponse<MutableList<TrendingResponse>>) {
        dataBinding.recyclerViewGrid.layoutManager =
            GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false)
        dataBinding.recyclerViewGrid.adapter = GridTrendingMovieAdapter(
            onDemoItemClick = { _, position ->
                gotoPage(
                    ListMovieFragmentDirections.gotoMovieDetailFragment(
                        MovieDetailContext(
                            movieId = trendingResponse.results?.get(position)?.id ?: 0,
                            imgPoster = trendingResponse.results?.get(position)?.posterPath ?: "",
                            movieName = trendingResponse.results?.get(position)?.originalTitle
                                ?: trendingResponse.results?.get(position)?.originalName ?: "",
                            date = trendingResponse.results?.get(position)?.releaseDate
                                ?: trendingResponse.results?.get(position)?.firstAirDate ?: "",
                            voteAvg = trendingResponse.results?.get(position)?.voteAverage ?: 0.0,
                            voteCount = trendingResponse.results?.get(position)?.voteCount ?: 0,
                            overview = trendingResponse.results?.get(position)?.overview ?: ""
                        )
                    )
                )
            }
        ).apply {
            add(trendingResponse.results ?: mutableListOf())
        }
    }

    private fun onRecyclerViewGridSearchLayout(searchMovieResponse: ApiPageResponse<MutableList<SearchMovieResponse>>) {
        dataBinding.recyclerViewList.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        dataBinding.recyclerViewGrid.adapter = GridSearchMovieAdapter(
            onDemoItemClick = { _, position ->
                gotoPage(
                    ListMovieFragmentDirections.gotoMovieDetailFragment(
                        MovieDetailContext(
                            movieId = searchMovieResponse.results?.get(position)?.id ?: 0,
                            imgPoster = searchMovieResponse.results?.get(position)?.posterPath
                                ?: "",
                            movieName = searchMovieResponse.results?.get(position)?.originalTitle
                                ?: "",
                            date = searchMovieResponse.results?.get(position)?.releaseDate ?: "",
                            voteAvg = searchMovieResponse.results?.get(position)?.voteAverage
                                ?: 0.0,
                            voteCount = searchMovieResponse.results?.get(position)?.voteCount ?: 0,
                            overview = searchMovieResponse.results?.get(position)?.overview ?: ""
                        )
                    )
                )
            }
        ).apply {
            add(searchMovieResponse.results ?: mutableListOf())
        }
    }
}

