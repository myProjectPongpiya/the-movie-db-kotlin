package th.co.project.the_movie_db.ui.component.list

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.load.model.GlideUrl
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.data.api.POSTER_BASE_URL
import th.co.project.the_movie_db.databinding.ItemMovieListBinding
import th.co.project.the_movie_db.databinding.ItemMovieSearchListBinding
import th.co.project.the_movie_db.databinding.ItemTrailerMovieBinding
import th.co.project.the_movie_db.extension.loadUrl
import th.co.project.the_movie_db.model.movie_db.search.SearchMovieResponse
import th.co.project.the_movie_db.model.movie_db.trending.TrendingResponse
import th.co.project.the_movie_db.ui.base.BaseRecyclerViewAdapter
import timber.log.Timber

class ListSearchMovieAdapter(
    private val onDemoItemClick: (demoResponse: SearchMovieResponse, position: Int) -> Unit,
) : BaseRecyclerViewAdapter<SearchMovieResponse, ItemMovieSearchListBinding>() {
    override fun createBinding(parent: ViewGroup): ItemMovieSearchListBinding {
        return ItemMovieSearchListBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    }

    override fun bind(
        binding: ItemMovieSearchListBinding,
        position: Int,
        item: SearchMovieResponse
    ) {
        binding.adapter = this
        binding.item = item
        binding.position = position

        checkImagePoster(binding, item)

        if (item.originalTitle.isNullOrEmpty()) binding.txtNameMovie.text = item.title
        else binding.txtNameMovie.text = item.originalTitle
        binding.textOverview.text = item.overview
    }

    fun onItemClick(item: SearchMovieResponse, position: Int) {
        onDemoItemClick.invoke(item, position)
    }

    private fun checkImagePoster(binding: ItemMovieSearchListBinding, item: SearchMovieResponse) {
        val imgPoster = POSTER_BASE_URL + item.posterPath
        if (item.posterPath.isNullOrEmpty()) {
            binding.image.setImageResource(R.drawable.img_logo_app)
        } else {
            item.posterPath.let {
                binding.image.loadUrl(
                    GlideUrl(imgPoster),
                    placeholderRes = R.drawable.img_logo_app,
                    onErrorRes = R.drawable.img_logo_app
                )
            }
        }
    }
}
