package th.co.project.the_movie_db.ui.component.splash

import android.view.animation.AnimationUtils
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import th.co.project.the_movie_db.MainActivity
import th.co.project.the_movie_db.R
import th.co.project.the_movie_db.databinding.FragmentSplashScreenBinding
import th.co.project.the_movie_db.extension.visible
import th.co.project.the_movie_db.ui.base.BaseFragment
import th.co.project.the_movie_db.ui.component.home.HomeScreenFragment

class SplashScreenFragment : BaseFragment<FragmentSplashScreenBinding, SplashScreenViewModel>() {

    override val layoutId: Int = R.layout.fragment_splash_screen

    override val viewModelClass: Class<SplashScreenViewModel>
        get() = SplashScreenViewModel::class.java

    var startTime = System.currentTimeMillis()


    override fun onFragmentStart() {
        dataBinding.fragment = this
//        AppPrefs.clearAllData()

        val animationFadeIn = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_in)
        val animationSlideInLeft =
            AnimationUtils.loadAnimation(requireContext(), R.anim.slide_in_left)
        val animationSlideUp =
            AnimationUtils.loadAnimation(requireContext(), R.anim.slide_up)

        dataBinding.imgLogo.startAnimation(animationFadeIn)
        GlobalScope.launch(Dispatchers.Main) {
            delay(500)
            dataBinding.txtTitle.visible()
            dataBinding.txtTitle.startAnimation(animationSlideInLeft)
            dataBinding.txtDemoApp.visible()
            dataBinding.txtDemoApp.startAnimation(animationSlideInLeft)
            GlobalScope.launch(Dispatchers.Main) {
                delay(500)
                dataBinding.txtSubTitle.visible()
                dataBinding.txtSubTitle.startAnimation(animationSlideUp)
            }
        }


        GlobalScope.launch(Dispatchers.Main) {
            delay(getDelay())

//            activity?.supportFragmentManager
//                ?.beginTransaction()
//                ?.replace(R.id.container, HomeScreenFragment())
//                ?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                ?.commit()

            gotoPage(
                SplashScreenFragmentDirections.gotoHomeScreenFragment()
            )
//            findNavController().popBackStack(R.id.splashFragment , true)
        }
    }

    private fun getDelay(): Long {
        return startTime?.let {
            5500 - (System.currentTimeMillis() - it)
        } ?: 5500
    }
}