package th.co.project.the_movie_db.usecase

import io.reactivex.Observable
import th.co.project.the_movie_db.data.api.TheMovieDBInterfaceAPI
import th.co.project.the_movie_db.model.movie_db.cast_credits.CastCreditsResponse
import javax.inject.Inject

class GetCastCreditsUseCase @Inject constructor(
    private val api: TheMovieDBInterfaceAPI,
) : BaseUseCase.WithParams<Int, CastCreditsResponse>() {
    override fun onExecute(params: Int): Observable<CastCreditsResponse> {
        return api.getCastCredits(params).map { response ->
            response
        }
    }
}