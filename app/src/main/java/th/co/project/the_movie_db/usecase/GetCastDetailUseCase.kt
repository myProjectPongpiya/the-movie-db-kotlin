package th.co.project.the_movie_db.usecase

import io.reactivex.Observable
import th.co.project.the_movie_db.data.api.TheMovieDBInterfaceAPI
import th.co.project.the_movie_db.model.movie_db.cast_detail.CastDetailResponse
import javax.inject.Inject

class GetCastDetailUseCase @Inject constructor(
    private val api: TheMovieDBInterfaceAPI,
) : BaseUseCase.WithParams<Int, CastDetailResponse>() {
    override fun onExecute(params: Int): Observable<CastDetailResponse> {
        return api.getCastDetail(params).map { response ->
            response
        }
    }
}