package th.co.project.the_movie_db.usecase

import io.reactivex.Observable
import th.co.project.the_movie_db.data.api.TheMovieDBInterfaceAPI
import th.co.project.the_movie_db.model.movie_db.credits.CreditsResponse
import javax.inject.Inject

class GetCreditsUseCase @Inject constructor(
    private val api: TheMovieDBInterfaceAPI,
) : BaseUseCase.WithParams<Int, CreditsResponse>() {
    override fun onExecute(params: Int): Observable<CreditsResponse> {
        return api.getMovieCredits(params).map { response ->
            response
        }
    }
}