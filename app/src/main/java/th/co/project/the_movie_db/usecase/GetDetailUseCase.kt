package th.co.project.the_movie_db.usecase

import io.reactivex.Observable
import th.co.project.the_movie_db.data.api.API_KEY
import th.co.project.the_movie_db.data.api.BASE_URL
import th.co.project.the_movie_db.data.api.SERVICE
import th.co.project.the_movie_db.data.api.TheMovieDBInterfaceAPI
import th.co.project.the_movie_db.model.movie_db.detail.MovieDetailsResponse
import timber.log.Timber
import javax.inject.Inject

class GetDetailUseCase @Inject constructor(
    private val api: TheMovieDBInterfaceAPI,
) : BaseUseCase.WithParams<Int, MovieDetailsResponse>() {
    override fun onExecute(params: Int): Observable<MovieDetailsResponse> {
        return api.getMovieDetails(params).map { response ->
            response
        }
    }
}