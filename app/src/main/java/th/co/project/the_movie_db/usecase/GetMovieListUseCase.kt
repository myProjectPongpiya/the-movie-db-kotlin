package th.co.project.the_movie_db.usecase

import io.reactivex.Observable
import th.co.project.the_movie_db.data.api.SERVICE
import th.co.project.the_movie_db.data.api.TheMovieDBInterfaceAPI
import th.co.project.the_movie_db.data.api.http.ApiPageResponse
import th.co.project.the_movie_db.model.movie_db.list.MovieList
import timber.log.Timber
import javax.inject.Inject

class GetMovieListUseCase @Inject constructor(
    private val api: TheMovieDBInterfaceAPI,
) : BaseUseCase.WithParams<Int, ApiPageResponse<MutableList<MovieList>>>() {
    override fun onExecute(params: Int): Observable<ApiPageResponse<MutableList<MovieList>>> {
        return api.getMovieList(params).map { response ->
            response
        }
    }
}