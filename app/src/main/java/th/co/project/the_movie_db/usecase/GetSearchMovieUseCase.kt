package th.co.project.the_movie_db.usecase

import io.reactivex.Observable
import th.co.project.the_movie_db.data.api.*
import th.co.project.the_movie_db.data.api.http.ApiPageResponse
import th.co.project.the_movie_db.model.movie_db.search.SearchMovieResponse
import th.co.project.the_movie_db.model.movie_db.search.SearchResponse
import javax.inject.Inject

class GetSearchMovieUseCase @Inject constructor(
    private val api: TheMovieDBInterfaceSearchAPI,
) : BaseUseCase.WithParams<String, ApiPageResponse<MutableList<SearchMovieResponse>>>() {
    override fun onExecute(params: String): Observable<ApiPageResponse<MutableList<SearchMovieResponse>>> {
        return api.getSearchMovie(params).map { response ->
            response
        }
    }
}