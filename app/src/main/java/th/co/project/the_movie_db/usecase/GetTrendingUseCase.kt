package th.co.project.the_movie_db.usecase

import io.reactivex.Observable
import th.co.project.the_movie_db.data.api.API_KEY
import th.co.project.the_movie_db.data.api.BASE_URL
import th.co.project.the_movie_db.data.api.SERVICE
import th.co.project.the_movie_db.data.api.TheMovieDBInterfaceAPI
import th.co.project.the_movie_db.data.api.http.ApiPageResponse
import th.co.project.the_movie_db.model.movie_db.trending.TrendingResponse
import timber.log.Timber
import javax.inject.Inject

class GetTrendingUseCase @Inject constructor(
    private val api: TheMovieDBInterfaceAPI,
) : BaseUseCase.WithParamsAny<String, String , ApiPageResponse<MutableList<TrendingResponse>>>() {
    override fun onExecute(params: String, params1: String): Observable<ApiPageResponse<MutableList<TrendingResponse>>> {
        return api.getTrending(params, params1).map { response ->
            response
        }
    }
}