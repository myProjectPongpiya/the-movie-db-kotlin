package th.co.project.the_movie_db.usecase

import io.reactivex.Observable
import th.co.project.the_movie_db.data.api.TheMovieDBInterfaceAPI
import th.co.project.the_movie_db.model.movie_db.videos.VideosResponse
import javax.inject.Inject

class GetVideosUseCase @Inject constructor(
    private val api: TheMovieDBInterfaceAPI,
) : BaseUseCase.WithParams<Int, VideosResponse>() {
    override fun onExecute(params: Int): Observable<VideosResponse> {
        return api.getVideos(params).map { response ->
            response
        }
    }
}